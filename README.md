# DuyBlog



## Giới Thiệu

Web blog gồm các chức năng cơ bản của 1 website blog gồm các vai trò:

Admin : Quản lí bài viết(xóa bài viết,duyệt bài),quản lí người dùng(phân quyền,xóa người dùng)   
Author: Quản lí bài viết của mình(thêm sửa xóa),quản lí bình luận       
Người dùng: đăng nhập(có thể đăng nhập bằng fb google), đăng kí, xem bình luận, tìm kiếm blog(theo từ khóa,thể loại, tác giả)

Hình ảnh minh họa:  
Trang Admin: 
![](./images/admin1.png)
![](./images/admin2.png)
Trang Tác Giả:

![](./images/author1.png)
![](./images/author2.png)

Trang đăng kí đăng nhập
![](./images/login.png)
![](./images/register.png)

Trang chủ và trang tin tức:

![](./images/user1.png)
![](./images/user2.png)
![](./images/blog1.png)
![](./images/blog2.png)
