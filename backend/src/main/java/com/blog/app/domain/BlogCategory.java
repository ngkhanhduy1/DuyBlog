package com.blog.app.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "blog_category")
public class BlogCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "blog_id")
    private Long blogId;

    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "category_name")
    private String categoryName;

    @ManyToOne
    @JoinColumn(name = "blog_id",insertable=false, updatable=false)
    private Blog blog;

    @ManyToOne
    @JoinColumn(name = "category_id",insertable=false, updatable=false)
    private Category category;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBlogId() {
        return blogId;
    }

    public void setBlogId(Long blogId) {
        this.blogId = blogId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BlogCategory)) return false;
        BlogCategory that = (BlogCategory) o;
        return Objects.equals(getId(), that.getId()) && Objects.equals(getBlogId(), that.getBlogId()) && Objects.equals(getCategoryId(), that.getCategoryId()) && Objects.equals(getBlog(), that.getBlog()) && Objects.equals(getCategory(), that.getCategory());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBlogId(), getCategoryId(), getBlog(), getCategory());
    }

    @Override
    public String toString() {
        return "BlogCategory{" +
            "id=" + id +
            ", blogId=" + blogId +
            ", categoryId=" + categoryId +
            ", categoryName='" + categoryName + '\'' +
            ", blog=" + blog +
            ", category=" + category +
            '}';
    }
}
