package com.blog.app.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "image")
public class Image implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "blog_id")
    private  Long blogId;

    @ManyToOne
    @JoinColumn(name = "blog_id",insertable=false, updatable=false)
    private Blog blog;

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Long getBlogId() {
        return blogId;
    }

    public void setBlogId(Long blogId) {
        this.blogId = blogId;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Image)) return false;
        Image image = (Image) o;
        return Objects.equals(getId(), image.getId()) && Objects.equals(getImageUrl(), image.getImageUrl()) && Objects.equals(getBlogId(), image.getBlogId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getImageUrl(), getBlogId());
    }

    @Override
    public String toString() {
        return "Image{" +
            "id=" + id +
            ", imageUrl='" + imageUrl + '\'' +
            ", blogId=" + blogId +
            '}';
    }
}
