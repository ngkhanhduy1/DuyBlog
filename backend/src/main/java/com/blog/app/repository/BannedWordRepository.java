package com.blog.app.repository;

import com.blog.app.domain.BannedWord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BannedWordRepository extends JpaRepository<BannedWord, Long> {
    List<BannedWord> findByName(String name);
}
