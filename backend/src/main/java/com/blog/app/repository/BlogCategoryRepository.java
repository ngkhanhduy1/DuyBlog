package com.blog.app.repository;

import com.blog.app.domain.BlogCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BlogCategoryRepository extends JpaRepository<BlogCategory,Long> {

    List<BlogCategory> findByBlogId(long id);

    List<BlogCategory> findByCategoryId(long id);

    List<BlogCategory> findByBlogIdAndCategoryName(Long blogId, String categoryName);


}
