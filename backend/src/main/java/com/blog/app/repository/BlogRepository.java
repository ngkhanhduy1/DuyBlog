package com.blog.app.repository;

import com.blog.app.domain.Blog;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data SQL repository for the Blog entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BlogRepository extends JpaRepository<Blog, Long> ,JpaSpecificationExecutor<Blog>{
//    List<Blog> findByCategoryId(long id);

    @Query(nativeQuery = true,
        value = "SELECT * FROM Blog b ORDER BY b.views DESC LIMIT 5")
    List<Blog> findTop5OrderByViews();

    @Query(nativeQuery = true,
        value = "SELECT * FROM Blog b WHERE b.user_id = ?1 ORDER BY b.views DESC LIMIT 4")
    List<Blog> findByAuthor(long id);

    


    @Query(nativeQuery = true,
        value = "SELECT * FROM Blog b WHERE b.status = ?1")
    List<Blog> findByStatus(String s);

    @Query(nativeQuery = true,
        value = "SELECT * FROM Blog b ORDER BY RAND() LIMIT 4")
    List<Blog> getRandomBlog();

    @Override
    List<Blog> findAll(Specification<Blog> specification);

    List<Blog> findAllByUserId(long id);

    @Query(nativeQuery = true,
        value = "SELECT * FROM Blog b inner join Blog_category bc on b.id = bc.blog_id where bc.category_id = ?1")
    List<Blog> findAllByCategoryId(long id);
}
