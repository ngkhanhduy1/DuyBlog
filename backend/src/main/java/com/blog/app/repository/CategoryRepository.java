package com.blog.app.repository;

import com.blog.app.domain.Blog;
import com.blog.app.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data SQL repository for the Category entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    List<Category> findByName(String s);

    @Query(nativeQuery = true,
        value = "SELECT distinct c.id,c.name,c.create_date FROM Category c inner join Blog_category bc on c.id = bc.category_id")
    List<Category> findAllCategory();
}
