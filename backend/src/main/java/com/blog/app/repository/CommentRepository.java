package com.blog.app.repository;

import com.blog.app.domain.Blog;
import com.blog.app.domain.BlogCategory;
import com.blog.app.domain.Comment;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data SQL repository for the Comment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findByBlogId(Long id);

    List<Comment> findByStatus(String id);

    List<Comment> findByBlogIdAndStatus(Long blogId, String status);

    @Query(nativeQuery = true,
        value = "SELECT * FROM Comment c inner join Blog b on c.blog_id = b.id where b.user_id = ?1")
    List<Comment> findByAuthorId(long id);



}
