package com.blog.app.repository;


import com.blog.app.domain.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {
    List<Image> findByBlogId(Long id);

    @Query(nativeQuery = true,
        value = "SELECT * FROM Image i inner join Blog b on i.blog_id = b.id where b.user_id = ?1")
    List<Image> findByAuthorId(long id);
}
