package com.blog.app.repository;

import com.blog.app.domain.Blog;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class SearchingCriteria {

    private String keyword;
    private String status;
    private LocalDate createDateStart;
    private LocalDate createDateEnd;

    private  long id;

    public SearchingCriteria(String keyword, String status, LocalDate createDateStart, LocalDate createDateEnd, long id) {

        this.keyword = keyword;
        this.status = status;
        this.createDateStart = createDateStart;
        this.createDateEnd = createDateEnd;
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDate getCreateDateStart() {
        return createDateStart;
    }

    public void setCreateDateStart(LocalDate createDateStart) {
        this.createDateStart = createDateStart;
    }

    public LocalDate getCreateDateEnd() {
        return createDateEnd;
    }

    public void setCreateDateEnd(LocalDate createDateEnd) {
        this.createDateEnd = createDateEnd;
    }

    public Specification<Blog> toSpecification() {
        return (root, query, criteriaBuilder) -> {
            Predicate[] predicateArr = (Predicate[]) this.buildPredicates(root, criteriaBuilder).toArray(new Predicate[0]);
            System.out.println(predicateArr.length);
            return criteriaBuilder.and(predicateArr);
        };
    }

    private List<Predicate> buildPredicates(Root<Blog> rootBlog, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        if (StringUtils.hasText(getStatus())) {
            System.out.println("=================> setting status" + getStatus());
            predicates.add(criteriaBuilder.equal(rootBlog.get("status"), getStatus()));
        }

        if (StringUtils.hasText(getKeyword())){
            System.out.println("=================> setting keyword" + getKeyword());
            predicates.add(criteriaBuilder.like(rootBlog.get("content"), "%"+getKeyword()+"%"));
        }

        if(getId() > 0){
            System.out.println("=================> setting keyword" + getId());
            predicates.add(criteriaBuilder.equal(rootBlog.get("userId"), getId()));
        }

        if(getCreateDateStart() != null){
            if(getCreateDateEnd() != null){
                System.out.println("=================> setting date" + getCreateDateStart());
                System.out.println("=================> setting date" + getCreateDateEnd());
                predicates.add(criteriaBuilder.between(rootBlog.get("createDate"),getCreateDateStart(),getCreateDateEnd()));
            }else {
                System.out.println("=================> setting date" + getCreateDateStart());
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(rootBlog.get("createDate"),getCreateDateStart()));
            }
        }else{
            if(getCreateDateEnd() != null){
                System.out.println("=================> setting date" + getCreateDateEnd());
                predicates.add(criteriaBuilder.lessThanOrEqualTo(rootBlog.get("createDate"),getCreateDateEnd()));
            }

        }

        return predicates;
    }
}
