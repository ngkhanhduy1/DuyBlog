package com.blog.app.repository;

import com.blog.app.domain.Authority;
import com.blog.app.domain.Blog;
import com.blog.app.domain.User;
import com.blog.app.domain.UserBannedWord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserBannedWordRepository extends JpaRepository<UserBannedWord, Long> {
    @Query(nativeQuery = true,
        value = "SELECT * FROM user_bannedword b WHERE b.user_id = ?1")
    List<UserBannedWord> findByAuthor(long id);

}
