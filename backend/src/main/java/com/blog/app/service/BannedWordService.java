package com.blog.app.service;

import com.blog.app.domain.BannedWord;
import com.blog.app.repository.BannedWordRepository;
import com.blog.app.service.dto.BannedWordDTO;
import com.blog.app.service.mapper.BannedWordMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BannedWordService {

    private final BannedWordRepository bannedWordRepository;

    private final BannedWordMapper bannedWordMapper;

    public BannedWordService(BannedWordRepository bannedWordRepository, BannedWordMapper bannedWordMapper) {
        this.bannedWordRepository = bannedWordRepository;
        this.bannedWordMapper = bannedWordMapper;
    }

    public BannedWordDTO save(BannedWordDTO bannedWordDTO) {
        BannedWord bannedWord = bannedWordMapper.toEntity(bannedWordDTO);
        bannedWord = bannedWordRepository.save(bannedWord);
        return bannedWordMapper.toDto(bannedWord);
    }

    public List<BannedWordDTO> findAll(){
        return bannedWordMapper.toDto(bannedWordRepository.findAll());
    }

    public void delete(long id){
        bannedWordRepository.deleteById(id);
    }

}
