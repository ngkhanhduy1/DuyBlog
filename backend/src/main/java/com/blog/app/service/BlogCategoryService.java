package com.blog.app.service;


import com.blog.app.domain.BlogCategory;
import com.blog.app.repository.BlogCategoryRepository;
import com.blog.app.service.dto.BlogCategoryDTO;
import com.blog.app.service.mapper.BlogCategoryMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BlogCategoryService {

    private final BlogCategoryRepository blogCategoryRepository;

    private final BlogCategoryMapper blogCategoryMapper;

    public BlogCategoryService(BlogCategoryRepository blogCategoryRepository, BlogCategoryMapper blogCategoryMapper){
        this.blogCategoryRepository = blogCategoryRepository;
        this.blogCategoryMapper = blogCategoryMapper;
    }

    @Transactional(readOnly = true)
    public List<BlogCategoryDTO> findAllByBlogId(long id) {
        return blogCategoryMapper.toDto(blogCategoryRepository.findByBlogId(id));
    }

    @Transactional(readOnly = true)
    public List<BlogCategoryDTO> findAllByCategoryId(long id) {
        return blogCategoryMapper.toDto(blogCategoryRepository.findByCategoryId(id));
    }

    public void deletebyBlogId(Long id) {
        List<BlogCategory> list = blogCategoryRepository.findByBlogId(id);
        for(BlogCategory l: list){
            blogCategoryRepository.deleteById(l.getId());
        }
    }
}
