package com.blog.app.service;

import com.blog.app.domain.Blog;
import com.blog.app.domain.BlogCategory;
import com.blog.app.domain.Category;
import com.blog.app.domain.Comment;
import com.blog.app.repository.*;
import com.blog.app.service.dto.BlogCategoryDTO;
import com.blog.app.service.dto.BlogDTO;

import com.blog.app.service.dto.SearchDTO;
import com.blog.app.service.mapper.BlogMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class BlogService {

    private final BlogRepository blogRepository;

    private final CategoryRepository categoryRepository;

    private final BlogCategoryRepository blogCategoryRepository;

    private final CommentRepository commentRepository;

    private final BlogMapper blogMapper;

    public BlogService(BlogRepository blogRepository, CategoryRepository categoryRepository,
                       BlogCategoryRepository blogCategoryRepository, CommentRepository commentRepository,
                       BlogMapper blogMapper) {
        this.blogRepository = blogRepository;
        this.categoryRepository = categoryRepository;
        this.blogCategoryRepository = blogCategoryRepository;
        this.commentRepository = commentRepository;
        this.blogMapper = blogMapper;
    }

    public BlogDTO save(BlogDTO blogDTO) {

        Blog blog = blogMapper.toEntity(blogDTO);
        blog = blogRepository.save(blog);
        for(String s: blogDTO.getListCategory()){
            if(categoryRepository.findByName(s).isEmpty()){
                Category category = new Category();
                category.setName(s);
                category.setCreateDate(LocalDate.now());
                category = categoryRepository.save(category);
                BlogCategory blogCategory = new BlogCategory();
                blogCategory.setBlogId(blog.getId());
                blogCategory.setCategoryId(category.getId());
                blogCategory.setCategoryName(s);
                blogCategory = blogCategoryRepository.save(blogCategory);
            }else{
                List<Category> list = categoryRepository.findByName(s);
                BlogCategory blogCategory = new BlogCategory();
                blogCategory.setBlogId(blog.getId());
                blogCategory.setCategoryId(list.get(0).getId());
                blogCategory.setCategoryName(s);
                blogCategory = blogCategoryRepository.save(blogCategory);
            }
        }

        return blogMapper.toDto(blog);
    }

    public BlogDTO update(BlogDTO blogDTO) {
        Blog blog = blogMapper.toEntity(blogDTO);
        blog = blogRepository.save(blog);
        List<String> categories = blogDTO.getListCategory();
        for(String s: categories){
            if(categoryRepository.findByName(s).isEmpty()){
                Category category = new Category();
                category.setName(s);
                category.setCreateDate(LocalDate.now());
                category = categoryRepository.save(category);
                BlogCategory blogCategory = new BlogCategory();
                blogCategory.setBlogId(blog.getId());
                blogCategory.setCategoryId(category.getId());
                blogCategory.setCategoryName(s);
                blogCategory = blogCategoryRepository.save(blogCategory);
            }else{
                List<Category> list = categoryRepository.findByName(s);
                BlogCategory blogCategory = new BlogCategory();
                blogCategory.setBlogId(blog.getId());
                blogCategory.setCategoryId(list.get(0).getId());
                blogCategory.setCategoryName(s);
                blogCategory = blogCategoryRepository.save(blogCategory);
            }
        }

        return blogMapper.toDto(blog);
    }

    public boolean check(Blog blog,List<String> list){
        for(String s: list){
            if(blogCategoryRepository.findByBlogIdAndCategoryName(blog.getId(), s).size() == 0)
                return false;
        }
        return true;
    }

    public long sumViews(){
        long view =0;
        for(Blog b: blogRepository.findAll()){
            view += b.getViews();
        }
        return view;
    }



    public List<BlogDTO> search(SearchDTO searchDTO){
        SearchingCriteria searchingCriteria = new SearchingCriteria(searchDTO.getKeyword(), searchDTO.getStatus(),
            searchDTO.getCreateDateStart(), searchDTO.getCreateDateEnd(), searchDTO.getId());
        List<Blog> blog = blogRepository.findAll(searchingCriteria.toSpecification());
        List<Blog> rs = new ArrayList<>();
        if(searchDTO.getCategories().size() > 0){
            for(Blog b: blog){
                if(check(b,searchDTO.getCategories())){
                    rs.add(b);
                }
            }
        }else{
            return blogMapper.toDto(blog);
        }
        List<BlogDTO> result = blogMapper.toDto(rs);
        return result;
    }

    public long sumViewsByDate(SearchDTO searchDTO){
        long view = 0;
        SearchingCriteria searchingCriteria = new SearchingCriteria(searchDTO.getKeyword(), searchDTO.getStatus(),
            searchDTO.getCreateDateStart(), searchDTO.getCreateDateEnd(), searchDTO.getId());
        List<Blog> blog = blogRepository.findAll(searchingCriteria.toSpecification());
        for(Blog b: blog){
            view += b.getViews();
        }
        return view;
    }


    @Transactional(readOnly = true)
    public List<BlogDTO> findAll() {
        return blogMapper.toDto(blogRepository.findAll());
    }

    public  List<BlogDTO> findByAuthorID(long id){
        return blogMapper.toDto(blogRepository.findAllByUserId(id));
    }

    @Transactional(readOnly = true)
    public  List<BlogDTO> getRandomBlog() {
        return blogMapper.toDto(blogRepository.getRandomBlog());
    }

    @Transactional(readOnly = true)
    public List<BlogDTO> top5OrderByView() {
        return blogMapper.toDto(blogRepository.findTop5OrderByViews());
    }

    @Transactional(readOnly = true)
    public Optional<BlogDTO> findOne(Long id) {
        return blogRepository.findById(id).map(blogMapper::toDto);
    }

    public void delete(Long id) {
        List<BlogCategory> list = blogCategoryRepository.findByBlogId(id);

        for(BlogCategory l: list){
            blogCategoryRepository.deleteById(l.getId());
        }

        List<Comment> comments = commentRepository.findByBlogId(id);
        for(Comment cmt: comments){
            commentRepository.deleteById(cmt.getId());
        }

        blogRepository.deleteById(id);
    }

    public void addCategory(BlogCategoryDTO blogCategoryDTO){
        if(categoryRepository.findByName(blogCategoryDTO.getCategoryName()).size() == 0){
            Category category = new Category();
            category.setName(blogCategoryDTO.getCategoryName());
            category.setCreateDate(LocalDate.now());
            category = categoryRepository.save(category);
            BlogCategory blogCategory = new BlogCategory();
            blogCategory.setBlogId(blogCategoryDTO.getBlogId());
            blogCategory.setCategoryId(category.getId());
            blogCategory.setCategoryName(blogCategoryDTO.getCategoryName());
            blogCategory = blogCategoryRepository.save(blogCategory);
        }else if(blogCategoryRepository.findByBlogIdAndCategoryName(blogCategoryDTO.getBlogId(), blogCategoryDTO.getCategoryName()).size() == 0){
            List<Category> list = categoryRepository.findByName(blogCategoryDTO.getCategoryName());
            BlogCategory blogCategory = new BlogCategory();
            blogCategory.setBlogId(blogCategoryDTO.getBlogId());
            blogCategory.setCategoryId(list.get(0).getId());
            blogCategory.setCategoryName(blogCategoryDTO.getCategoryName());
            blogCategory = blogCategoryRepository.save(blogCategory);
        }

    }

    public BlogDTO viewUp(long id) {
        Blog blog = blogRepository.getById(id);
        long view = blog.getViews();
        blog.setViews(view+1);
        blog = blogRepository.save(blog);
        return blogMapper.toDto(blog);
    }

    public void setPrivate(long id){
        Blog blog = blogRepository.getById(id);
        blog.setStatus("Riêng tư");
        blogRepository.save(blog);
    }

    public void setPublic(long id){
        Blog blog = blogRepository.getById(id);
        blog.setStatus("Đã công bố");
        blogRepository.save(blog);
    }

    public void schedulePost(){
        LocalDate today = LocalDate.now();
        List<Blog> blogs = blogRepository.findByStatus("Chờ đăng bài");
        for(Blog b: blogs){
            if(today.isEqual(b.getCreateDate())){
                b.setStatus("Đã công bố");
                blogRepository.save(b);
            }
        }
    }

    public List<BlogDTO> getAllPublicBlog() {

        return blogMapper.toDto(blogRepository.findByStatus("Đã công bố"));
    }

    public List<BlogDTO> suggestByAuthor(long blogId) {
        long id = blogRepository.getById(blogId).getUserId();
        return blogMapper.toDto(blogRepository.findByAuthor(id));
    }

    public List<BlogDTO> findByBlogCategoryId(long id) {
        return blogMapper.toDto(blogRepository.findAllByCategoryId(id));
    }
}
