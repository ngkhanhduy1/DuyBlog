package com.blog.app.service;

import com.blog.app.domain.Category;
import com.blog.app.repository.CategoryRepository;
import com.blog.app.service.dto.CategoryDTO;
import com.blog.app.service.mapper.CategoryMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class CategoryService {

    private final CategoryRepository categoryRepository;

    private final CategoryMapper categoryMapper;

    public CategoryService(CategoryRepository categoryRepository, CategoryMapper categoryMapper) {
        this.categoryRepository = categoryRepository;
        this.categoryMapper = categoryMapper;
    }


    public CategoryDTO save(CategoryDTO categoryDTO) {
        Category category = categoryMapper.toEntity(categoryDTO);
        category = categoryRepository.save(category);
        return categoryMapper.toDto(category);
    }

    public CategoryDTO update(CategoryDTO categoryDTO) {
        Category category = categoryMapper.toEntity(categoryDTO);
        category = categoryRepository.save(category);
        return categoryMapper.toDto(category);
    }


    @Transactional(readOnly = true)
    public List<CategoryDTO> findAll() {
        List<Category> category = categoryRepository.findAllCategory();
        List<CategoryDTO> categoryDto = categoryMapper.toDto(category);
        return categoryDto;
    }

    public List<String> findAllName() {
        List<Category> category = categoryRepository.findAllCategory();
        List<String> categoryList = new ArrayList<>();
        for(Category c: category){
            categoryList.add(c.getName());
        }

        return categoryList;
    }

    @Transactional(readOnly = true)
    public Optional<CategoryDTO> findOne(Long id) {
        return categoryRepository.findById(id).map(categoryMapper::toDto);
    }

    public void delete(Long id) {
        categoryRepository.deleteById(id);
    }
}
