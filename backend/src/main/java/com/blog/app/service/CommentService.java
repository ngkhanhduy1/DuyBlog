package com.blog.app.service;

import com.blog.app.domain.Blog;
import com.blog.app.domain.Comment;
import com.blog.app.repository.BlogRepository;
import com.blog.app.repository.CommentRepository;
import com.blog.app.service.dto.CommentDTO;
import com.blog.app.service.mapper.CommentMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Comment}.
 */
@Service
@Transactional
public class CommentService {

    private final CommentRepository commentRepository;

    private final CommentMapper commentMapper;


    public CommentService(CommentRepository commentRepository, CommentMapper commentMapper, BlogRepository blogRepository) {
        this.commentRepository = commentRepository;
        this.commentMapper = commentMapper;
    }

    public CommentDTO save(CommentDTO commentDTO) {
        Comment comment = commentMapper.toEntity(commentDTO);
        comment = commentRepository.save(comment);
        return commentMapper.toDto(comment);
    }

    public List<CommentDTO> findAll(){
        return commentMapper.toDto(commentRepository.findAll());
    }

    public Optional<CommentDTO> findById(long id){
        return commentRepository.findById(id).map(commentMapper::toDto);
    }

    public CommentDTO update(CommentDTO commentDTO) {
        Comment comment = commentMapper.toEntity(commentDTO);
        comment = commentRepository.save(comment);
        return commentMapper.toDto(comment);
    }

    public CommentDTO addSpamStatus(long id) {
        Comment comment = commentRepository.getById(id);
        comment.setStatus("spam");
        comment = commentRepository.save(comment);
        return commentMapper.toDto(comment);
    }

    public CommentDTO deleteContent(long id) {
        Comment comment = commentRepository.getById(id);
        comment.setContent("Bình luận này đã bị quản trị viên xóa nội dung");
        comment = commentRepository.save(comment);
        return commentMapper.toDto(comment);
    }

    public CommentDTO restoreContent(long id) {
        Comment comment = commentRepository.getById(id);
        String content = comment.getContentBackup();
        comment.setContent(content);
        comment = commentRepository.save(comment);
        return commentMapper.toDto(comment);
    }

    public CommentDTO removeSpamStatus(long id) {
        Comment comment = commentRepository.getById(id);
        comment.setStatus("public");
        comment = commentRepository.save(comment);
        return commentMapper.toDto(comment);
    }

    public void delete(long id){
        commentRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public List<CommentDTO> findByBlogId(long id){
        return commentMapper.toDto(commentRepository.findByBlogId(id));
    }

    public List<CommentDTO> findByStatus(String status){
        return commentMapper.toDto(commentRepository.findByStatus(status));
    }

    public List<CommentDTO> findByBlogIdAndStatus(long id,String status ){
        return commentMapper.toDto(commentRepository.findByBlogIdAndStatus(id,status));
    }

    public List<CommentDTO> getCommentbyAuthorId(long id){
        return commentMapper.toDto(commentRepository.findByAuthorId(id));
    }
}
