package com.blog.app.service;


import com.blog.app.domain.Image;
import com.blog.app.repository.ImageRepository;
import com.blog.app.service.dto.ImageDTO;
import com.blog.app.service.mapper.ImageMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ImageService {

    private final ImageRepository imageRepository;

    private final ImageMapper imageMapper;

    public ImageService(ImageRepository imageRepository, ImageMapper imageMapper) {
        this.imageRepository = imageRepository;
        this.imageMapper = imageMapper;
    }

    public ImageDTO save(ImageDTO imageDTO){
        Image image = new Image();
        image.setImageUrl(imageDTO.getImageUrl());
        image.setBlogId(imageDTO.getBlogId());
        image = imageRepository.save(image);
        return imageMapper.toDto(image);
    }

    public List<ImageDTO> findAll() {
        return imageMapper.toDto(imageRepository.findAll());
    }

    public List<ImageDTO> findByBlogId(long id) {
        return imageMapper.toDto(imageRepository.findByBlogId(id));
    }


    public void deleteByBlogId(long id){
        List<Image> list = imageRepository.findByBlogId(id);
        for(Image l: list){
            imageRepository.deleteById(l.getId());
        }
    }

    public List<ImageDTO> getCommentbyAuthorId(long id){
        return imageMapper.toDto(imageRepository.findByAuthorId(id));
    }

}
