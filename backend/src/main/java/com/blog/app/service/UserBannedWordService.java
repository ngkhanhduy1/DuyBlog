package com.blog.app.service;

import com.blog.app.domain.BannedWord;
import com.blog.app.domain.UserBannedWord;
import com.blog.app.repository.BlogCategoryRepository;
import com.blog.app.repository.UserBannedWordRepository;
import com.blog.app.service.dto.BannedWordDTO;
import com.blog.app.service.dto.UserBannedWordDTO;
import com.blog.app.service.mapper.BlogCategoryMapper;
import com.blog.app.service.mapper.UserBannedWordMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserBannedWordService {
    private final UserBannedWordRepository userBannedWordRepository;

    private final UserBannedWordMapper userBannedWordMapper;

    public UserBannedWordService(UserBannedWordRepository userBannedWordRepository, UserBannedWordMapper userBannedWordMapper) {
        this.userBannedWordRepository = userBannedWordRepository;
        this.userBannedWordMapper = userBannedWordMapper;
    }

    public List<UserBannedWordDTO> findAll(){
        return userBannedWordMapper.toDto(userBannedWordRepository.findAll());
    }

    public List<UserBannedWordDTO> findAllAuthor(long id){
        return userBannedWordMapper.toDto(userBannedWordRepository.findByAuthor(id));
    }

    public UserBannedWordDTO save(UserBannedWordDTO userBannedWordDTO) {
        UserBannedWord userBannedWord = userBannedWordMapper.toEntity(userBannedWordDTO);
        userBannedWord = userBannedWordRepository.save(userBannedWord);
        return userBannedWordMapper.toDto(userBannedWord);
    }

}
