package com.blog.app.service.dto;

import java.io.Serializable;
import java.util.Objects;

public class BlogCategoryDTO implements Serializable {
    private Long id;

    private Long blogId;

    private Long categoryId;

    private String categoryName;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBlogId() {
        return blogId;
    }

    public void setBlogId(Long blogId) {
        this.blogId = blogId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }


    @Override
    public String toString() {
        return "BlogCategoryDTO{" +
            "id=" + id +
            ", blogId=" + blogId +
            ", categoryId=" + categoryId +
            ", categoryName='" + categoryName + '\'' +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BlogCategoryDTO)) return false;
        BlogCategoryDTO that = (BlogCategoryDTO) o;
        return Objects.equals(getId(), that.getId()) && Objects.equals(getBlogId(), that.getBlogId()) && Objects.equals(getCategoryId(), that.getCategoryId()) && Objects.equals(getCategoryName(), that.getCategoryName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBlogId(), getCategoryId(), getCategoryName());
    }
}
