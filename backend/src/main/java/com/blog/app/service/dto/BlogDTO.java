package com.blog.app.service.dto;

import com.blog.app.domain.BlogCategory;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.blog.app.domain.User;

import javax.persistence.Lob;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * A DTO for the {@link com.blog.app.domain.Blog} entity.
 */
public class BlogDTO implements Serializable {

    private Long id;

    private String title;

    @Lob
    private String summary;

    @Lob
    private String content;

    private Long views;

    private String status;

    private LocalDate createDate;

    private LocalDate updateDate;

    private Long userId;

    private User user;

    private String image;

    @JsonIgnore
    private List<BlogCategory> blogCategories;

    private List<String> listCategory;

    private List<String> categories;

    public List<String> getListCategory() {
        return listCategory;
    }

    public void setListCategory(List<String> listCategory) {
        this.listCategory = listCategory;
    }

    public List<String> getCategories() {
        int i =0;
        List<String> list = new ArrayList<String>();
        for(BlogCategory blogCategory: blogCategories){
            i++;
            list.add(blogCategory.getCategoryName());
        }
        if(i==0){
            list.add("Không có nhãn");
        }
        return list;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public List<BlogCategory> getBlogCategories() {
        return blogCategories;
    }

    public void setBlogCategories(List<BlogCategory> blogCategories) {
        this.blogCategories = blogCategories;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getViews() {
        return views;
    }

    public void setViews(Long views) {
        this.views = views;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BlogDTO)) return false;
        BlogDTO blogDTO = (BlogDTO) o;
        return Objects.equals(getId(), blogDTO.getId()) && Objects.equals(getTitle(),
            blogDTO.getTitle()) && Objects.equals(getSummary(), blogDTO.getSummary()) && Objects.equals(getContent(),
            blogDTO.getContent()) && Objects.equals(getViews(), blogDTO.getViews()) && Objects.equals(getStatus(),
            blogDTO.getStatus()) && Objects.equals(getCreateDate(), blogDTO.getCreateDate()) && Objects.equals(getUpdateDate(),
            blogDTO.getUpdateDate()) && Objects.equals(getUserId(), blogDTO.getUserId()) && Objects.equals(getUser(),
            blogDTO.getUser()) && Objects.equals(getBlogCategories(),
            blogDTO.getBlogCategories()) && Objects.equals(getListCategory(),
            blogDTO.getListCategory()) && Objects.equals(getCategories(), blogDTO.getCategories());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle(), getSummary(), getContent(), getViews(), getStatus(), getCreateDate(),
            getUpdateDate(), getUserId(), getUser(), getBlogCategories(), getListCategory(), getCategories());
    }

    @Override
    public String toString() {
        return "BlogDTO{" +
            "id=" + id +
            ", title='" + title + '\'' +
            ", summary='" + summary + '\'' +
            ", content='" + content + '\'' +
            ", views=" + views +
            ", status='" + status + '\'' +
            ", createDate=" + createDate +
            ", updateDate=" + updateDate +
            ", categories=" + categories +
            ", user=" + user +
            '}';
    }
}
