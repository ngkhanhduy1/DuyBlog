package com.blog.app.service.dto;

import com.blog.app.domain.Blog;
import com.blog.app.domain.User;


import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link com.blog.app.domain.Comment} entity.
 */
public class CommentDTO implements Serializable {

    private Long id;

    private Long blogId;

    private Long userId;

    private String content;

    private String status;

    private String contentBackup;

    private LocalDate createDate;

    private User user;

    private Blog blog;

    public Blog getBlog() {
        return blog;
    }

    public String getContentBackup() {
        return contentBackup;
    }

    public void setContentBackup(String contentBackup) {
        this.contentBackup = contentBackup;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBlogId() {
        return blogId;
    }

    public void setBlogId(Long blogId) {
        this.blogId = blogId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommentDTO)) return false;
        CommentDTO that = (CommentDTO) o;
        return Objects.equals(getId(), that.getId()) && Objects.equals(getBlogId(),
            that.getBlogId()) && Objects.equals(getUserId(), that.getUserId()) && Objects.equals(getContent(),
            that.getContent()) && Objects.equals(getCreateDate(), that.getCreateDate()) && Objects.equals(getUser(),
            that.getUser());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBlogId(), getUserId(), getContent(), getCreateDate(), getUser());
    }

    @Override
    public String toString() {
        return "CommentDTO{" +
            "id=" + id +
            ", blogId=" + blogId +
            ", userId=" + userId +
            ", content='" + content + '\'' +
            ", createDate=" + createDate +
            ", user=" + user +
            '}';
    }
}
