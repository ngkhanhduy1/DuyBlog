package com.blog.app.service.dto;

import com.blog.app.domain.Blog;

import java.io.Serializable;
import java.util.Objects;

public class ImageDTO implements Serializable {
    private Long id;
    private String imageUrl;
    private Long blogId;
    private Blog blog;

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Long getBlogId() {
        return blogId;
    }

    public void setBlogId(Long blogId) {
        this.blogId = blogId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ImageDTO)) return false;
        ImageDTO imageDTO = (ImageDTO) o;
        return Objects.equals(getId(), imageDTO.getId()) && Objects.equals(getImageUrl(), imageDTO.getImageUrl()) && Objects.equals(getBlogId(), imageDTO.getBlogId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getImageUrl(), getBlogId());
    }

    @Override
    public String toString() {
        return "ImageDTO{" +
            "id=" + id +
            ", imageUrl='" + imageUrl + '\'' +
            ", blogId=" + blogId +
            '}';
    }
}
