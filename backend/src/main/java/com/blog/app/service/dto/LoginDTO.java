package com.blog.app.service.dto;

import com.blog.app.domain.User;

/**
 * A DTO representing a user, with only the public attributes.
 */
public class LoginDTO {

    private Long id;

    private String login;

    public LoginDTO() {
        // Empty constructor needed for Jackson.
    }

    public LoginDTO(User user) {
        this.id = user.getId();
        // Customize it here if you need, or not, firstName/lastName/etc
        this.login = user.getLogin();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserDTO{" +
            "id='" + id + '\'' +
            ", login='" + login + '\'' +
            "}";
    }
}
