package com.blog.app.service.dto;

import java.time.LocalDate;
import java.util.List;

public class SearchDTO {

    private List<String> categories;

    private long id;

    private String keyword;
    private String status;
    private LocalDate createDateStart;
    private LocalDate createDateEnd;

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDate getCreateDateStart() {
        return createDateStart;
    }

    public void setCreateDateStart(LocalDate createDateStart) {
        this.createDateStart = createDateStart;
    }

    public LocalDate getCreateDateEnd() {
        return createDateEnd;
    }

    public void setCreateDateEnd(LocalDate createDateEnd) {
        this.createDateEnd = createDateEnd;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
