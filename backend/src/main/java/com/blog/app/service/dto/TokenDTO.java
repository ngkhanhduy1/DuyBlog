package com.blog.app.service.dto;

public class TokenDTO {
    String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
