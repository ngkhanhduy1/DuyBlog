package com.blog.app.service.mapper;

import com.blog.app.domain.BannedWord;
import com.blog.app.service.dto.BannedWordDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BannedWordMapper extends EntityMapper<BannedWordDTO, BannedWord> {
}
