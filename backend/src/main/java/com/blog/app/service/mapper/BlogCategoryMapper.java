package com.blog.app.service.mapper;

import com.blog.app.domain.BlogCategory;
import com.blog.app.service.dto.BlogCategoryDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BlogCategoryMapper extends EntityMapper<BlogCategoryDTO, BlogCategory>{ }
