package com.blog.app.service.mapper;

import com.blog.app.domain.Blog;
import com.blog.app.service.dto.BlogDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BlogMapper extends EntityMapper<BlogDTO, Blog> {}
