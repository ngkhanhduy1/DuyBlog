package com.blog.app.service.mapper;

import com.blog.app.domain.Category;
import com.blog.app.service.dto.CategoryDTO;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface CategoryMapper extends EntityMapper<CategoryDTO, Category> {}
