package com.blog.app.service.mapper;

import com.blog.app.domain.Comment;
import com.blog.app.service.dto.CommentDTO;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface CommentMapper extends EntityMapper<CommentDTO, Comment> {}
