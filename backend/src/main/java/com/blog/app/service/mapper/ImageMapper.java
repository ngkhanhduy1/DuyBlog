package com.blog.app.service.mapper;

import com.blog.app.domain.Image;
import com.blog.app.service.dto.ImageDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ImageMapper extends EntityMapper<ImageDTO, Image> {
}
