package com.blog.app.service.mapper;

import com.blog.app.domain.UserBannedWord;
import com.blog.app.service.dto.UserBannedWordDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserBannedWordMapper extends EntityMapper<UserBannedWordDTO, UserBannedWord>{
}
