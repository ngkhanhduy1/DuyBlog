package com.blog.app.web.rest;

import com.blog.app.repository.UserRepository;
import com.blog.app.service.UserService;
import com.blog.app.service.dto.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class AccountResource {

    private static class AccountResourceException extends RuntimeException {

        private AccountResourceException(String message) {
            super(message);
        }
    }

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    private final UserRepository userRepository;

    private final UserService userService;

    public AccountResource(UserRepository userRepository, UserService userService) {
        this.userRepository = userRepository;
        this.userService = userService;
    }

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity registerAccount(@Valid @RequestBody UserDTO userDTO, BindingResult result) {

        if (result.hasErrors()) {
            for(ObjectError e: result.getAllErrors()){
                return ResponseEntity.ok().body(e.getDefaultMessage());
            }
        }

        if(userRepository.findOneByLogin(userDTO.getLogin().toLowerCase()).isPresent()){
            return ResponseEntity.ok().body("Tài khoản đã tồn tại");
        }

        if(userRepository.findOneByEmailIgnoreCase(userDTO.getEmail()).isPresent()){
            return ResponseEntity.ok().body("Email đã tồn tại");
        }

        userService.registerUser(userDTO, userDTO.getPassword());

        return ResponseEntity.ok().body("Đăng kí thành công");
    }


    @GetMapping("/authenticate")
    public ResponseEntity isAuthenticated() {
        log.debug("REST request to check if the current user is authenticated");
        UserDTO user = userService
                            .getUserWithAuthorities()
                            .map(UserDTO::new)
                            .orElseThrow(() -> new AccountResourceException("User could not be found"));
        Set<String> roles = user.getAuthorities();
        return ResponseEntity.ok().body(roles);
    }

    @GetMapping("/account")
    public UserDTO getAccount() {
        return userService
            .getUserWithAuthorities()
            .map(UserDTO::new)
            .orElseThrow(() -> new AccountResourceException("User could not be found"));
    }

}
