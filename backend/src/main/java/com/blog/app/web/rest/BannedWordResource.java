package com.blog.app.web.rest;


import com.blog.app.repository.BannedWordRepository;
import com.blog.app.service.BannedWordService;
import com.blog.app.service.dto.BannedWordDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class BannedWordResource {

    private final BannedWordService bannedWordService;
    private final BannedWordRepository bannedWordRepository;

    public BannedWordResource(BannedWordService bannedWordService, BannedWordRepository bannedWordRepository) {
        this.bannedWordService = bannedWordService;
        this.bannedWordRepository = bannedWordRepository;
    }

    @PostMapping("/bannedwords")
    public ResponseEntity<BannedWordDTO> addBannedWord(@RequestBody BannedWordDTO bannedWordDTO){
        BannedWordDTO result = new BannedWordDTO();
        if(bannedWordRepository.findByName(bannedWordDTO.getName()).size() == 0){
            result = bannedWordService.save(bannedWordDTO);
        }else {
            result.setName("Từ Khóa đã tồn tại");
            return ResponseEntity.ok().body(result);
        }
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/bannedwords")
    public ResponseEntity<List<BannedWordDTO>> getAllBannedWord(){
        List<BannedWordDTO> result = bannedWordService.findAll();
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/bannedwords/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id){
        bannedWordService.delete(id);
        return ResponseEntity.ok().body("xoa thanh cong");
    }


}
