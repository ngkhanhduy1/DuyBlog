package com.blog.app.web.rest;

import com.blog.app.repository.BlogCategoryRepository;
import com.blog.app.service.BlogCategoryService;
import com.blog.app.service.dto.BlogCategoryDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class BlogCategoryResource {

    public static class BlogCategoryResourceException extends RuntimeException{
        private BlogCategoryResourceException(String message){super(message);}
    }

    private final BlogCategoryRepository blogCategoryRepository;

    private final BlogCategoryService blogCategoryService;

    public BlogCategoryResource(BlogCategoryRepository blogCategoryRepository, BlogCategoryService blogCategoryService) {
        this.blogCategoryRepository = blogCategoryRepository;
        this.blogCategoryService = blogCategoryService;
    }

    @GetMapping("/blogcategories/findbyblogid/{id}")
    public ResponseEntity<List<BlogCategoryDTO>> getAllByBlogId(@PathVariable Long id) {
        List<BlogCategoryDTO> result = blogCategoryService.findAllByBlogId(id);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/blogcategories/findbycategoryid/{id}")
    public ResponseEntity<List<BlogCategoryDTO>> getAllByCategoryId(@PathVariable Long id) {
        List<BlogCategoryDTO> result = blogCategoryService.findAllByCategoryId(id);
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/blogcategories/deletebyblogid/{id}")
    public ResponseEntity<Void> deleteBlog(@PathVariable Long id) {
        blogCategoryService.deletebyBlogId(id);
        return ResponseEntity.ok().build();
    }
}
