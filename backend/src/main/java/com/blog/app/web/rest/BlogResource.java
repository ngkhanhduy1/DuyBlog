package com.blog.app.web.rest;


import com.blog.app.service.BlogService;
import com.blog.app.service.dto.BlogCategoryDTO;
import com.blog.app.service.dto.BlogDTO;
import com.blog.app.service.dto.SearchDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tech.jhipster.web.util.ResponseUtil;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")

public class BlogResource {

    private static final Path IMAGE_FOLDER = Paths.get("E:\\DuyBlog\\frontend\\src\\assets");

    private final BlogService blogService;

    public BlogResource(BlogService blogService) {
        this.blogService = blogService;
    }

    @PostMapping("/blogs")
    public ResponseEntity<BlogDTO> createBlog(@RequestBody BlogDTO blogDTO){
        BlogDTO result = blogService.save(blogDTO);
        return ResponseEntity.ok().body(result);
    }

    @PutMapping("/blogs/{id}")
    public ResponseEntity<BlogDTO> updateBlog(@RequestBody BlogDTO blogDTO){
        BlogDTO result = blogService.update(blogDTO);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/blogs")
    public ResponseEntity<List<BlogDTO>> getAllBlogs() {
        List<BlogDTO> result = blogService.findAll();
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/blogs/findbyauthorid/{id}")
    public ResponseEntity<List<BlogDTO>> getByAuthorId(@PathVariable long id) {
        List<BlogDTO> result = blogService.findByAuthorID(id);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/blogs/count")
    public  ResponseEntity<Integer> getCount(){
        int result = blogService.findAll().size();
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/blogs/sumview")
    public  ResponseEntity<Long>  getSumViews(){
        long a = blogService.sumViews();
        return ResponseEntity.ok().body(a);
    }

    @GetMapping("/blogs/order")
    public ResponseEntity<List<BlogDTO>> top5OrderByViews() {
        List<BlogDTO> result = blogService.top5OrderByView();
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/blogs/search")
    public ResponseEntity<List<BlogDTO>> search(@RequestBody SearchDTO searchDTO ){
        List<BlogDTO> result = blogService.search(searchDTO);
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/blogs/sumviewbydate")
    public  ResponseEntity<Long>  getSumViewsByDate(@RequestBody SearchDTO searchDTO ){
        long a = blogService.sumViewsByDate(searchDTO);
        return ResponseEntity.ok().body(a);
    }


    @GetMapping("/blogs/random")
    public  ResponseEntity<List<BlogDTO>> getRandomBlog() {
        List<BlogDTO> result = blogService.getRandomBlog();
        return  ResponseEntity.ok().body(result);
    }

    @GetMapping("/blogs/{id}")
    public ResponseEntity<BlogDTO> getBlog(@PathVariable Long id) {
        Optional<BlogDTO> blogDTO = blogService.findOne(id);
        return ResponseUtil.wrapOrNotFound(blogDTO);
    }

    @DeleteMapping("/blogs/{id}")
    public ResponseEntity<Void> deleteBlog(@PathVariable Long id) {
        blogService.delete(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/blogs/addcategory")
    public ResponseEntity<Void> addCategory(@RequestBody BlogCategoryDTO blogCategoryDTO) {
        blogService.addCategory(blogCategoryDTO);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/blogs/viewup/{id}")
    public ResponseEntity<BlogDTO> viewUp(@PathVariable long id){
        BlogDTO result = blogService.viewUp(id);
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/uploadimage")
    public ResponseEntity<String> create(
                      @RequestPart MultipartFile image) throws IOException {
        Path imagePath = Paths.get("images");
        if (!Files.exists(IMAGE_FOLDER.resolve(imagePath))) {
            Files.createDirectories(IMAGE_FOLDER.resolve(imagePath));
        }
        Path file = IMAGE_FOLDER
            .resolve(imagePath).resolve(image.getOriginalFilename());
        try (OutputStream os = Files.newOutputStream(file)) {
            os.write(image.getBytes());
        }
        return ResponseEntity.ok().body(file.toString());
    }

    @PutMapping("/blogs/setprivate/{id}")
    public  ResponseEntity<String> setPrivate(@PathVariable long id){
        blogService.setPrivate(id);
        return ResponseEntity.ok().body("ok");
    }

    @PutMapping("/blogs/setpublic/{id}")
    public  ResponseEntity<String> setPublic(@PathVariable long id){
        blogService.setPublic(id);
        return ResponseEntity.ok().body("ok");
    }

    @PutMapping("/blogs/schedulepost")
    @Scheduled(cron = "0 0 16 ? * *")
    public  ResponseEntity<String> schdulePost(){
        blogService.schedulePost();
        return ResponseEntity.ok().body("ok");
    }

    @GetMapping("/blogs/getpublicblog")
    public  ResponseEntity<List<BlogDTO>> getPublicBlog() {
        List<BlogDTO> result = blogService.getAllPublicBlog();
        return  ResponseEntity.ok().body(result);
    }

    @GetMapping("/blogs/suggestbyAuthor/{blogid}")
    public  ResponseEntity<List<BlogDTO>> getByAuthor(@PathVariable long blogid){
        List<BlogDTO> result = blogService.suggestByAuthor(blogid);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/blogs/findbycategoryid/{blogid}")
    public  ResponseEntity<List<BlogDTO>> getByCategoryId(@PathVariable long blogid){
        List<BlogDTO> result = blogService.findByBlogCategoryId(blogid);
        return ResponseEntity.ok().body(result);
    }
}
