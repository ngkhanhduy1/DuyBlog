package com.blog.app.web.rest;

import com.blog.app.domain.UserBannedWord;
import com.blog.app.domain.BannedWord;
import com.blog.app.repository.BannedWordRepository;
import com.blog.app.repository.UserBannedWordRepository;
import com.blog.app.service.CommentService;
import com.blog.app.service.dto.CommentDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")

public class CommentResource {

    private final CommentService commentService;

    private final BannedWordRepository bannedWordRepository;

    private final UserBannedWordRepository userBannedWordRepository;



    public CommentResource(CommentService commentService, BannedWordRepository bannedWordRepository,UserBannedWordRepository userBannedWordRepository) {
        this.commentService = commentService;
        this.bannedWordRepository = bannedWordRepository;
        this.userBannedWordRepository = userBannedWordRepository;

    }

    @PostMapping("/comments/{id}")
    public ResponseEntity<String> createComment(@Valid @RequestBody CommentDTO commentDTO,@PathVariable long id){

//        List<BannedWord> list = bannedWordRepository.findAll();
//        for(BannedWord b : list){
//            if(commentDTO.getContent().contains(b.getName())){
//                return ResponseEntity.ok().body("bình luận chưa từ ngữ vi phạm tiêu chuẩn cộng đồng : " + b.getName());
//            }
//        }

        List<UserBannedWord> c = userBannedWordRepository.findByAuthor(id);
        for(UserBannedWord b : c){
            if(commentDTO.getContent().contains(b.getName())){
                return ResponseEntity.ok().body("bình luận chưa từ ngữ vi phạm tiêu chuẩn cộng đồng : " + b.getName());
            }
        }
        CommentDTO result = commentService.save(commentDTO);
        return ResponseEntity.ok().body("thêm thành công");
    }

    @GetMapping("/comments")
    public ResponseEntity<List<CommentDTO>> getAllComment(){
        List<CommentDTO> result = commentService.findAll();
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/comments/count")
    public  ResponseEntity<Integer> getCount(){
        int result = commentService.findAll().size();
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/comments/{id}")
    public  ResponseEntity<Optional<CommentDTO>> findById(@PathVariable long id){
        Optional<CommentDTO> result = commentService.findById(id);
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/comments/{id}")
    public ResponseEntity<String> deleteComment(@PathVariable Long id) {
        commentService.delete(id);
        return ResponseEntity.ok().body("xoa thanh cong");
    }

    @PutMapping("/comments/{id}")
    public ResponseEntity<CommentDTO> updateComment(@RequestBody CommentDTO commentDTO){
        CommentDTO result = commentService.update(commentDTO);
        return ResponseEntity.ok().body(result);
    }

    @PutMapping("/comments/addspam/{id}")
    public ResponseEntity<CommentDTO> addSpamStatus(@PathVariable long id){
        CommentDTO result = commentService.addSpamStatus(id);
        return ResponseEntity.ok().body(result);
    }

    @PutMapping("/comments/removespam/{id}")
    public ResponseEntity<CommentDTO> removeSpamStatus(@PathVariable long id){
        CommentDTO result = commentService.removeSpamStatus(id);
        return ResponseEntity.ok().body(result);
    }

    @PutMapping("/comments/deletecontent/{id}")
    public ResponseEntity<CommentDTO> deleteContent(@PathVariable long id){
        CommentDTO result = commentService.deleteContent(id);
        return ResponseEntity.ok().body(result);
    }

    @PutMapping("/comments/restorecontent/{id}")
    public ResponseEntity<CommentDTO> restoreContent(@PathVariable long id){
        CommentDTO result = commentService.restoreContent(id);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/comments/findbyblogid/{id}")
    public  ResponseEntity<List<CommentDTO>> findByBlogId(@PathVariable long id){
        List<CommentDTO> result = commentService.findByBlogId(id);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/comments/filterbyblogid/{id}")
    public ResponseEntity<List<CommentDTO>> filterByBlogId(@PathVariable long id ){
        return ResponseEntity.ok().body(commentService.findByBlogId(id));
    }

    @GetMapping("/comments/filterbystatus/{status}")
    public ResponseEntity<List<CommentDTO>> filterByStatus(@PathVariable String status ){
        return ResponseEntity.ok().body(commentService.findByStatus(status));
    }

    @PostMapping("/comments/filter")
    public ResponseEntity<List<CommentDTO>> filter(@RequestBody CommentDTO commentDTO){
        return ResponseEntity.ok().body(commentService.findByBlogIdAndStatus(commentDTO.getBlogId(),commentDTO.getStatus()));
    }

    @GetMapping("/comments/getbyauthorid/{id}")
    public  ResponseEntity<List<CommentDTO>> getbyauthorid(@PathVariable long id){
        return  ResponseEntity.ok().body(commentService.getCommentbyAuthorId(id));
    }


}
