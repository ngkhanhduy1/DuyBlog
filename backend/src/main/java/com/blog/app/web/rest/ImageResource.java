package com.blog.app.web.rest;

import com.blog.app.service.ImageService;
import com.blog.app.service.dto.ImageDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ImageResource {

    private final ImageService imageService;

    public ImageResource(ImageService imageService) {
        this.imageService = imageService;
    }

    @PostMapping("/images")
    public ResponseEntity<ImageDTO> create(@RequestBody ImageDTO imageDTO){
        ImageDTO result = imageService.save(imageDTO);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/images")
    public ResponseEntity<List<ImageDTO>> getAll(){
        List<ImageDTO> result = imageService.findAll();
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/images/findbyblogid/{id}")
    public ResponseEntity<List<ImageDTO>> getByBlogId(@PathVariable long id){
        List<ImageDTO> result = imageService.findByBlogId(id);
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/images/{id}")
    public  ResponseEntity<Void> deleteByBlogid(@PathVariable Long id){
        imageService.deleteByBlogId(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/images/getbyauthorid/{id}")
    public  ResponseEntity<List<ImageDTO>> getbyauthorid(@PathVariable long id){
        return  ResponseEntity.ok().body(imageService.getCommentbyAuthorId(id));
    }
}
