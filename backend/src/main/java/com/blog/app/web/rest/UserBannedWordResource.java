package com.blog.app.web.rest;

import com.blog.app.repository.BlogCategoryRepository;
import com.blog.app.repository.UserBannedWordRepository;
import com.blog.app.service.BlogCategoryService;
import com.blog.app.service.UserBannedWordService;
import com.blog.app.service.dto.BlogCategoryDTO;
import com.blog.app.service.dto.BlogDTO;
import com.blog.app.service.dto.UserBannedWordDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UserBannedWordResource {

    private final UserBannedWordRepository userBannedWordRepository;

    private final UserBannedWordService userBannedWordService;

    public UserBannedWordResource(UserBannedWordRepository userBannedWordRepository, UserBannedWordService userBannedWordService) {
        this.userBannedWordRepository = userBannedWordRepository;
        this.userBannedWordService = userBannedWordService;
    }

    @GetMapping("/test")
    public ResponseEntity<List<UserBannedWordDTO>> getAll() {
        List<UserBannedWordDTO> result = userBannedWordService.findAll();
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/test/{id}")
    public ResponseEntity<List<UserBannedWordDTO>> getAllByAuthor(@PathVariable long id) {
        List<UserBannedWordDTO> result = userBannedWordService.findAllAuthor(id);
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/test/{25}")
    public ResponseEntity<UserBannedWordDTO> createBlog(@RequestBody UserBannedWordDTO userBannedWordDTO){
        UserBannedWordDTO result = userBannedWordService.save(userBannedWordDTO);
        return ResponseEntity.ok().body(result);
    }
}
