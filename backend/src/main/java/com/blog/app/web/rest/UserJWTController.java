package com.blog.app.web.rest;

import com.blog.app.repository.UserRepository;
import com.blog.app.security.jwt.JWTFilter;
import com.blog.app.security.jwt.TokenProvider;
import com.blog.app.service.UserService;
import com.blog.app.service.dto.TokenDTO;
import com.blog.app.service.dto.UserDTO;
import com.blog.app.web.rest.vm.LoginVM;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.User;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.io.IOException;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class UserJWTController {

    private final TokenProvider tokenProvider;

    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    private final UserRepository userRepository;

    private final UserService userService;

    public UserJWTController(TokenProvider tokenProvider, AuthenticationManagerBuilder authenticationManagerBuilder,
                             UserRepository userRepository, UserService userService) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
            loginVM.getUsername(),
            loginVM.getPassword()
        );

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.createToken(authentication);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
    }

    @PostMapping("/facebook")
    public ResponseEntity<?> facebook(@RequestBody TokenDTO tokenDTO) {
        Facebook facebook = new FacebookTemplate(tokenDTO.getValue());
        final String [] fields = {"email","name","first_name","last_name"};
        User user = facebook.fetchObject("me", User.class, fields);
        if(userRepository.existsByEmail(user.getEmail())){
            LoginVM loginVM = new LoginVM();
            loginVM.setUsername(user.getId());
            loginVM.setPassword("123456");
            return authorize(loginVM);
        }else {
            UserDTO userDTO = new UserDTO();
            userDTO.setLogin(user.getId());
            userDTO.setEmail(user.getEmail());
            userDTO.setFirstName(user.getLastName());
            userDTO.setLastName(user.getFirstName());
            userService.registerUser(userDTO,"123456");
            LoginVM loginVM = new LoginVM();
            loginVM.setUsername(user.getId());
            loginVM.setPassword("123456");
            return authorize(loginVM);
        }
    }

    @PostMapping("/google")
    public ResponseEntity<?> google(@RequestBody TokenDTO tokenDTO) throws IOException{
        final NetHttpTransport transport = new NetHttpTransport();
        final JacksonFactory jacksonFactory = JacksonFactory.getDefaultInstance();
        GoogleIdTokenVerifier.Builder verifier =
            new GoogleIdTokenVerifier.Builder(transport, jacksonFactory)
                .setAudience(Collections.singletonList("53498111056-j16luq7ujs7hecj7c63324iuhf2uqoc0.apps.googleusercontent.com"));
        final GoogleIdToken googleIdToken = GoogleIdToken.parse(verifier.getJsonFactory(), tokenDTO.getValue());
        final GoogleIdToken.Payload payload = googleIdToken.getPayload();
        if(userRepository.existsByEmail(payload.getEmail())){
            LoginVM loginVM = new LoginVM();
            loginVM.setUsername(userRepository.findOneByEmail(payload.getEmail()).getLogin());
            loginVM.setPassword("123456");
            return authorize(loginVM);
        }else {
            UserDTO userDTO = new UserDTO();
            userDTO.setLogin(payload.get("sub").toString());
            userDTO.setEmail(payload.getEmail());
            userDTO.setFirstName(payload.get("given_name").toString());
            userDTO.setLastName(payload.get("family_name").toString());
            userService.registerUser(userDTO,"123456");
            LoginVM loginVM = new LoginVM();
            loginVM.setUsername(payload.get("sub").toString());
            loginVM.setPassword("123456");
            return authorize(loginVM);
        }
    }

    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }
}
