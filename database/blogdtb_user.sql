-- MySQL dump 10.13  Distrib 8.0.29, for Win64 (x86_64)
--
-- Host: localhost    Database: blogdtb
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(191) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `reset_date` timestamp NULL DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_user_login` (`login`),
  UNIQUE KEY `ux_user_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC','Administrator','Administrator','admin@localhost','system',NULL,NULL,'system',NULL),(2,'user','$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K','User','User','user@localhost','system',NULL,NULL,'system',NULL),(15,'nguyenduy21','$2a$10$i8lseArnNP1kE/3Yl9SxSOkbPCmY5Qxdf8eiQVXu0S/a.miK/CWoG','Nguyen','Duy','ngkhanhduy3123@gmailcom','anonymousUser','2022-05-30 20:30:06',NULL,'anonymousUser','2022-05-30 20:30:06'),(25,'khanhduy','$2a$10$OyzRSe9MIwF2nH4B.04YXeywRW0Ff5teYFDXHCmhx7aplCtU6qD7y','Khánh','Duy','ngkhanhduy341@gmail.com','anonymousUser','2022-06-15 21:59:15',NULL,'anonymousUser','2022-06-15 21:59:15'),(28,'khanh@@duy','$2a$10$Kd4tiP0ZTi.ateuUIoIf6Ou9pKeqfqnWOcKwA7aLm8ktQVT5zYtni','Khánh','Duy','ngkhanhduy@gmail.com','anonymousUser','2022-06-15 22:39:03',NULL,'anonymousUser','2022-06-15 22:39:03'),(32,'1206612663463986','$2a$10$gN935dZFuT8IfgRe8FSuuOrHZiC0ZR/l/RydroC5RKvQZlmJUjN6G','Nguyễn','Khánh Duy','ngkhanhduy1@gmail.com','anonymousUser','2022-07-19 12:04:33',NULL,'anonymousUser','2022-07-19 12:04:33'),(33,'101157607282317212028','$2a$10$pN5q4ll/ADWcqWouTwxWsOhEWIuT36vGlCLLYSjOmAxCnSh4W.mce','Duy','Nguyễn khánh','duycoi152000@gmail.com','anonymousUser','2022-07-20 12:21:55',NULL,'anonymousUser','2022-07-20 12:21:55'),(42,'anhduy789asd','$2a$10$U4.hgIeK1lkpy2jkS/T6Uut8O68Mo7UVGnM8d1EcbXSf.zkd2ZcT2','anhduy','anhduy','dss@adasd.com','anonymousUser','2022-08-07 08:36:49',NULL,'anonymousUser','2022-08-07 08:36:49'),(43,'quangkhanh','$2a$10$1K0iUhTKXPcBi/gtu0G6N.FUKRoE8CAWcaGEwB/kcyeDp3m30oT8i','Quang Khánh','Nguyễn','quangkhanh123@gmail.com','anonymousUser','2022-08-07 08:39:58',NULL,'anonymousUser','2022-08-07 08:39:58');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-08-08 12:14:51
