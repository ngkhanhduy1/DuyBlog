import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/services/account.service';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  constructor(
    private AccountService: AccountService,
    private modal: NzModalService,
    private message: NzMessageService
  ) { }
  
  public isDeleteSuccess: any;

  ngOnInit(): void {
    this.isDeleteSuccess = localStorage.getItem('isDeleteSuccess');
    if(this.isDeleteSuccess == 'true'){
      this.message.create('success', `Đã xóa tài khoản`);
      localStorage.setItem('isDeleteSuccess', "false");
    }
    this.getAllAccount()
  }

  public accounts: any;
  public totalAccount:  number = 0;
  public page: number =1 ;

  getAllAccount(){
    this.AccountService.getAllAccount().subscribe((res: any)=>{
      this.accounts = res;
      this.totalAccount = res.totalAccount;
      console.log(this.accounts)
    })
  }

  pageChangeEvent(event: number){
    this.page = event;
    this.getAllAccount();
  }
  delete(login: any){
    this.AccountService.delete(login).subscribe((res:any)=>{
      localStorage.setItem('isDeleteSuccess', "true");
      location.href = "/admin/account";
    })
  }
  showConfirm(login: any): void {

    this.modal.confirm({
      nzTitle: 'Xác nhận xóa tài khoản này?',
      nzOkText: 'Có',
      nzOkType: 'primary',
      nzOkDanger: true,
      nzOnOk: () => this.delete(login),
      nzCancelText: 'Không',
      nzOnCancel: () => console.log('Cancel')
    });
  }

}
