import { Component, OnInit  } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  
  isCollapsed = false;
  inputValue: string | null = null;
  constructor(
    private AuthService:AuthService
  ) { }

  public currentAcc: any;
  public role: any;

  ngOnInit(): void {
    this.role = localStorage.getItem('role');
    console.log(this.role);
    this.getUserByToken();
  
  }





  click(){
    let sidebar = document.querySelector("nav");
    console.log(sidebar);
    let getStatus = localStorage.getItem("status");
    if (getStatus && getStatus === "close") {
      sidebar?.classList.toggle("close");
      localStorage.setItem("status", "open");
    }else{
      sidebar?.classList.toggle("close");
      localStorage.setItem("status", "close");
    }
  }

  getUserByToken(){
    this.AuthService.getUserByToken().subscribe((res: any)=>{
      if(res){
        console.log(res);
        this.currentAcc = res;
        localStorage.setItem('user_id',res.id)
      }
    })
  }

  onEnter(){
    if(this.inputValue != null){
      localStorage.setItem('keyword',this.inputValue );
    }
    localStorage.setItem('isSearch', "true");
    location.href="/admin/blog/list"
  }

  logout(){
    localStorage.removeItem('token');
    window.location.href='/login'
  }

}

