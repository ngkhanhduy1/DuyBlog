import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { Routes, RouterModule } from '@angular/router';
import { ListBlogComponent } from './blog/list-blog/list-blog.component';
import { NzInputModule } from 'ng-zorro-antd/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { UpdateBlogComponent } from './blog/update-blog/update-blog.component';
import { AccountComponent } from './account/account.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import {MultiSelectModule} from 'primeng/multiselect';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { CKEditorModule } from 'ng2-ckeditor';
import { CommentComponent } from './comment/comment.component';
import { UserComponent } from './user/user.component';
import { ImageComponent } from './image/image.component';
import { BannedwordComponent } from './bannedword/bannedword.component';

const pagesRoutes: Routes = [
  {path: '', component: AdminComponent,children:[
    {
      path:'analytic',
      component:AnalyticsComponent
    },
    {
      path:'blog/list',
      component:ListBlogComponent
    },

    {
      path:'blog/update/:id',
      component:UpdateBlogComponent
    },
    {
      path:'account',
      component:AccountComponent
    },
    {
      path:'comment',
      component:CommentComponent
    },
    {
      path:'user',
      component:UserComponent
    },
    {
      path:'image',
      component:ImageComponent
    },
    {
      path:'bannedword',
      component:BannedwordComponent
    }
  ]}
]


@NgModule({
  declarations: [
    ListBlogComponent,
    UpdateBlogComponent,
    AccountComponent,
    AnalyticsComponent,
    CommentComponent,
    UserComponent,
    ImageComponent,
    BannedwordComponent
  ],
  imports: [
    FormsModule,
    NzInputModule,
    CommonModule,
    ReactiveFormsModule,
    NzDatePickerModule,
    NzModalModule,
    MultiSelectModule,
    NzSelectModule,
    CKEditorModule,
    NzMessageModule,
    NzButtonModule,
    RouterModule.forChild(pagesRoutes)
  ]
})
export class AdminModule { }
