import { Component, OnInit,ViewChild  } from '@angular/core';
import { BlogService } from 'src/app/services/blog.service';
import { CommentService } from 'src/app/services/comment.service';
import { Chart,registerables } from 'chart.js';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit {

  constructor(
    private Blogservice: BlogService,
    private CommentService: CommentService
  ) { }
  public date = new Date().toISOString().split("T")[0];
  public totalBlog: any;
  public totalView: any;
  public totalComment: any;
  public blogs: any;
  
  ngOnInit(): void {
    this.getTotalBlog();
    this.getTotalView();
    this.getTotalComment();
    this.getBlog()
  }

  getBlog(){
    this.Blogservice.top5orderByView().subscribe((res:any) =>{
      this.blogs = res;
      console.log(this.blogs)
    })
  }

  getTotalBlog(){
    this.Blogservice.count().subscribe((res: any)=>{
      this.totalBlog = res;
    })
  }

  getTotalComment(){
    this.CommentService.count().subscribe((res: any)=>{
      this.totalComment = res;
    })
  }

  getTotalView(){
    this.Blogservice.sumView().subscribe((res: any)=>{
      this.totalView = res;
    })
  }

  click(){
    let sidebar = document.querySelector("nav");
    console.log(sidebar);


    let getStatus = localStorage.getItem("status");
    if (getStatus && getStatus === "close") {
      sidebar?.classList.toggle("close");
      localStorage.setItem("status", "open");
    }else{
      sidebar?.classList.toggle("close");
      localStorage.setItem("status", "close");
    }

    
  }

}
