import { Component, OnInit } from '@angular/core';
import { BannedwordService } from 'src/app/services/bannedword.service';
import { TestService } from 'src/app/services/test.service';

@Component({
  selector: 'app-bannedword',
  templateUrl: './bannedword.component.html',
  styleUrls: ['./bannedword.component.scss']
})
export class BannedwordComponent implements OnInit {

  constructor(
    private BannedwordService:BannedwordService,
    private TestService:TestService
  ) { }

  public words: any;
  checkList: any;
  value: any;

  ngOnInit(): void {
    this.getAll();
  }

  getAll(){
    let role = localStorage.getItem('role');
    let userId = localStorage.getItem('user_id')
    if(role == 'author'){
      console.log('tacgia'+ userId);
      this.TestService.getAll(userId).subscribe((res: any) =>{
        console.log(res)
        this.words = res;
      })
    }else{
      this.BannedwordService.getAll().subscribe((res: any) =>{
        this.words = res;
      })
    }
    
  }

  checkbox(){
    var checklist =[]
    var checkboxAll1 = document.getElementsByTagName('input');
    var checkAllbtn = document.getElementsByClassName('checkall-btn')
    var count = 0;
    var sumCheckbox =0;
    for(var i=2; i < checkboxAll1.length; i++) {
      if(checkboxAll1[i].type == 'checkbox' ) {
        sumCheckbox++;
        if(checkboxAll1[i].checked == true){
          count++;
          checklist.push(checkboxAll1[i].value)
        }
      }
    }
    this.checkList = checklist
    console.log(this.checkList);
    if(sumCheckbox == count){
      checkboxAll1[1].checked = true;
    }else{
      checkboxAll1[1].checked = false;
    }
    if(count > 0){
      checkAllbtn[0].classList.remove('disabled')
    }else{
      checkAllbtn[0].classList.add('disabled')
    }
  }

  checkboxAll(){
    var checklist =[]
    var checkboxAll1 = document.getElementsByTagName('input')
    var count = 0
    console.log(checkboxAll1[2])

    for(var i=2; i < checkboxAll1.length; i++) {
      if(checkboxAll1[i].type == 'checkbox') {
        checkboxAll1[i].checked = checkboxAll1[2].checked;
        if(checkboxAll1[i].checked == true){
          count ++;
          checklist.push(checkboxAll1[i].value)
        }

      }
    }

    this.checkList = checklist
    console.log(this.checkList);

    var checkAllbtn = document.getElementsByClassName('checkall-btn')
    if(checkboxAll1[2].checked == true){
      checkAllbtn[0].classList.remove('disabled')
    }
    if(count == 0){
      checkAllbtn[0].classList.add('disabled')
    }
  }

  submit(){
    for(let a of this.checkList){
      this.BannedwordService.delete(a).subscribe((res: any) =>{

      })
    }
    location.reload()
  }

  add(){
    var part = this.value?.split(",");
    let role = localStorage.getItem('role');
    let userId = localStorage.getItem('user_id')
    if(role == 'author'){
      for(let p of part){
        console.log(p);
        let word ={
          name: p,
          userId:userId
        }
        this.TestService.create(word,userId).subscribe((res: any) =>{
  
        })
        location.reload()
      }
    }else{
      for(let p of part){
        console.log(p);
        let word ={
          name: p
        }
        this.BannedwordService.create(word).subscribe((res: any) =>{
  
        })
        location.reload()
      }
    }
    
  }
}
