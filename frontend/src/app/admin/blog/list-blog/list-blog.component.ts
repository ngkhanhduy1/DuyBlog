import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../../services/blog.service';
import { CategoryService } from 'src/app/services/category.service';
import { UserService } from 'src/app/services/user.service';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzMessageService } from 'ng-zorro-antd/message';
import {  UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-list-blog',
  templateUrl: './list-blog.component.html',
  styleUrls: ['./list-blog.component.scss']
})

export class ListBlogComponent implements OnInit {

  constructor(private BlogService:BlogService,
    private UserService:UserService,
    private CategoryService: CategoryService,
    private modal: NzModalService,
    private message: NzMessageService,
    ) { }
    
  listOfOption : any;
  listOfSelectedValue: string[] = [];
  public categories: any;
  public blogs: any;
  checkList: any;
  listCategory: any;
  public isSearch: any;
  public totalBlog: any;
  public count: any;
  isVisible = false;
  inputValue: string | null = null;
  public role: any;
  public isAdmin?: boolean;
  public authors: any;

  ngOnInit(): void {
    this.filter();
    this.isAdminOrAuthor();
    this.getAllCetagory();
    console.log(this.blogs)
  }

  isAdminOrAuthor(){
    this.role = localStorage.getItem('role');
    if(this.role == 'admin'){
      this.getAllAuthor();
      this.isAdmin = true;
    }else{
      this.isAdmin = false;
    }
  }

  getAllAuthor(){
    this.UserService.getAllAuthor().subscribe((res:any)=>{
      this.authors = res;
      console.log(res)
    })
  }

  filter(){
    this.isSearch = localStorage.getItem('isSearch')
    var c = localStorage.getItem('categories')
    var part = c?.split(",");
    var part1 =c?.split(",")[0];
    if(part1 == ''){
      part = [];
    }
    var date1 = localStorage.getItem('createDateStart')
    if(date1 == 'null'){
      date1 = '';
    }
    var date2 = localStorage.getItem('createDateEnd')
    if(date2 == 'null'){
      date2 = '';
    }
    var status1 = localStorage.getItem('status')
    var keyword = localStorage.getItem('keyword')
    this.role = localStorage.getItem('role');
    var id: any;
    if(this.role == 'author'){
      var user_id = localStorage.getItem('user_id');
      id = user_id;
    }else{
      id = localStorage.getItem('author_id');;
    }
    if(this.isSearch == 'true'){
      localStorage.setItem('isSearch', "false");
      this.searchForm.setValue({
        status:status1,
        keyword:keyword,
        createDateStart:localStorage.getItem('createDateStartForm'),
        createDateEnd:localStorage.getItem('createDateEndForm'),
        categories:part,
        id: id
      })
      
      let form = {
        status:status1,
        keyword:keyword,
        createDateStart:date1?.split("T")[0],
        createDateEnd:date2?.split("T")[0],
        categories:part,
        id: id
      }
      console.log(form)
      this.search(form)
      localStorage.setItem('keyword', '');
      localStorage.setItem('categories', '');
      localStorage.setItem('status', '');
      localStorage.setItem('createDateStart', '');
      localStorage.setItem('createDateEnd', '');
      localStorage.setItem('createDateStartForm', '');
      localStorage.setItem('createDateEndForm', '');
      localStorage.setItem('author_id', '0');

    }else{
      this.role = localStorage.getItem('role');
      if(this.role == 'admin'){
        this.getAllBlog();
      }else{
        var user_id = localStorage.getItem('user_id');
        this.getBlogByUserid(user_id);
      }
      
    }
  }

  checkboxAll(){
    var checklist =[]
    var checkboxAll1 = document.getElementsByTagName('input')
    var count = 0
    console.log(checkboxAll1[1])

    for(var i=2; i < checkboxAll1.length; i++) {
      if(checkboxAll1[i].type == 'checkbox') {
        checkboxAll1[i].checked = checkboxAll1[1].checked;
        if(checkboxAll1[i].checked == true){
          count ++;
          checklist.push(checkboxAll1[i].value)
        }

      }
    }

    this.checkList = checklist
    console.log(this.checkList);

    var checkAllbtn = document.getElementsByClassName('checkall-btn')
    if(checkboxAll1[1].checked == true){
      checkAllbtn[0].classList.remove('disabled')
    }
    if(count == 0){
      checkAllbtn[0].classList.add('disabled')
    }
    
  }

  checkbox(){
    var checklist =[]
    var checkboxAll1 = document.getElementsByTagName('input');
    var checkAllbtn = document.getElementsByClassName('checkall-btn')
    var count = 0;
    var sumCheckbox =0;
    for(var i=2; i < checkboxAll1.length; i++) {
      if(checkboxAll1[i].type == 'checkbox' ) {
        sumCheckbox++;
        if(checkboxAll1[i].checked == true){
          count++;
          checklist.push(checkboxAll1[i].value)
        }
      }
    }
    this.checkList = checklist
    console.log(this.checkList);
    if(sumCheckbox == count){
      checkboxAll1[1].checked = true;
    }else{
      checkboxAll1[1].checked = false;
    }
    if(count > 0){
      checkAllbtn[0].classList.remove('disabled')
    }else{
      checkAllbtn[0].classList.add('disabled')
    }
  }

  getAllBlog(){
    this.BlogService.getAllBlog().subscribe((res: any)=>{
      this.blogs = res;
    })
  }

  getBlogByUserid(id: any){
    this.BlogService.finbyAuthorId(id).subscribe((res: any)=>{
      this.blogs = res;
    })
  }

  delete(id: any){
    this.BlogService.delete(id).subscribe((res:any)=>{
      console.log("da xoa")
    })
  }

  setPrivate(id: any){
    this.BlogService.setPrivate(id).subscribe((res:any)=>{
      console.log("da an bai viet")
    })
  }

  setPublic(id: any){
    this.BlogService.setPublic(id).subscribe((res:any)=>{
      console.log("da xuat ban bai viet")
    })
  }

  submit(){
    var select = document.getElementsByTagName('select');
    console.log(select[0].value)
    console.log(this.checkList)
    if(select[0].value == 'delete'){
      for(let a of this.checkList){
        this.delete(a)
      }
      location.href = "/admin/blog/list";
    }else if(select[0].value == 'addcategory'){
      this.showModal()
    }else if(select[0].value == 'setPrivate'){
      for(let a of this.checkList){
        this.setPrivate(a)
      }
      location.href = "/admin/blog/list";
    }else if(select[0].value == 'setPublic'){
      for(let a of this.checkList){
        this.setPublic(a)
      }
      location.href = "/admin/blog/list";
    }
  }

  searchForm = new UntypedFormGroup({
    keyword:new UntypedFormControl('',[]),
    status: new UntypedFormControl('',[]),
    createDateStart: new UntypedFormControl('', []),
    createDateEnd: new UntypedFormControl('', []),
    categories: new UntypedFormControl('',[]),
    id: new UntypedFormControl('',[])
  });

  submitForm(){
    console.log(this.searchForm.value)
    var date1 = this.searchForm.get('createDateStart')?.value;
    var date1_form = this.searchForm.get('createDateStart')?.value
    var date2 = this.searchForm.get('createDateEnd')?.value;
    var date2_form = this.searchForm.get('createDateEnd')?.value;
    console.log(date1);
    console.log(date2);
    var s ='';
    
    if(!date1){
      
    }else{
      date1 = this.searchForm.get('createDateStart')?.value.toISOString().split("T")[0];
    }

    if(!date2){
      
    }else{
      console.log('abcd')
      date2 = this.searchForm.get('createDateEnd')?.value.toISOString().split("T")[0];
    }
    localStorage.setItem('isSearch', "true");
    localStorage.setItem('status', this.searchForm.get('status')?.value);
    localStorage.setItem('author_id', this.searchForm.get('id')?.value)
    localStorage.setItem('createDateStart', date1);
    localStorage.setItem('createDateEnd', date2);
    localStorage.setItem('createDateStartForm', date1_form);
    localStorage.setItem('createDateEndForm', date2_form);
    var cate = this.searchForm.get('categories')?.value;
    var count =0
    for(let c of cate){
      s+= c;
      count++
      if(count< cate.length){
        s+=','
      }
      
    }
    localStorage.setItem('categories', s);
    window.location.reload()
  }

  search(form: any){
    this.BlogService.search(form).subscribe((res: any)=>{
      console.log(res)
      this.blogs = res;
    })
  }

  getAllCetagory(){
    this.CategoryService.getAllCategoryName().subscribe((res: any) =>{
      this.listOfOption = res;
    })
  }
  

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    console.log(this.inputValue)
    this.listCategory = this.inputValue?.split(",")
    for(let category of this.listCategory){
      if(category.trim().length === 0){
        console.log('loai')
      }else{
        for(let l of this.checkList){
          let form = {
            blogId: l,
            categoryName: category
          }
          console.log(form)
          this.addCategory(form)
        }
      }
    }
    this.isVisible = false;
    window.location.reload()
  }
  addCategory(form: any){
    this.BlogService.addCategory(form).subscribe((res: any)=>{
      console.log('abc')
    })
  }
  handleCancel(): void {
    
    this.isVisible = false;
  }

  onEnter(string: any){
    
  }

  addBlog(){
    var date1 = new Date().toISOString().split("T")[0];
    var date2 = new Date().toISOString().split("T")[0];
    let form = {
      content: '',
      createDate: date1,
      summary: '',
      title: '',
      updateDate: date2,
      views: 0,
      status:"bản nháp",
      listCategory:[] 
    }
    this.BlogService.create(form).subscribe((res: any)=>{
      console.log(res)
      var url = "/admin/blog/update/" + res.id.toString()
      location.href =url
    })
  }
}
