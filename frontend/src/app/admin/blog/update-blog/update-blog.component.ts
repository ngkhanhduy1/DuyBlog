import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../../services/blog.service';
import { CategoryService } from '../../../services/category.service';
import { ImageService } from 'src/app/services/image.service';
import {  UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders, HttpResponse, } from '@angular/common/http';



@Component({
  selector: 'app-update-blog',
  templateUrl: './update-blog.component.html',
  styleUrls: ['./update-blog.component.scss'],

})
export class UpdateBlogComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private ImageService:ImageService,
    private CategoryService:CategoryService,
    private BlogService:BlogService,
    private message: NzMessageService,
    private http: HttpClient
  ) { }
  
  selectedFile:any;
  ckeditorContent: string = '';
  public blog: any;
  public id: any;
  public category: any;
  date1 = null;
  public role: any;
  public isAdmin?: boolean;

  public date = new Date().toISOString().split("T")[0];

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.getBlogById(this.id);
    this.role = localStorage.getItem('role');
    if(this.role == 'admin'){
      this.isAdmin = true;
    }else{
      this.isAdmin = false;
    }
   
  }

  getBlogById(id: any){
    this.BlogService.findById(id).subscribe((res:any)=>{
      console.log(res);
      localStorage.setItem('views',res.views)
      localStorage.setItem('createDate',res.createDate)
      var categories = res.categories;
      var string_category = '';
      var count = 0;
      for(let category of categories){
        if(count == 0){
          string_category= string_category +category
        }else{
          string_category= string_category+ ',' +category
        }
        count++
      }
      console.log(string_category)
      this.blogForm.setValue({
        title:res.title,
        summary:res.summary,
        categories: string_category,
        content:res.content,
        scheduleDate: '',
      })
    })
  }

  blogForm = new UntypedFormGroup({
    title: new UntypedFormControl('',[]),
    summary: new UntypedFormControl('', []),
    categories: new UntypedFormControl('', []),
    content: new UntypedFormControl('', []),
    scheduleDate: new UntypedFormControl('',[])
  });

  submit(){
    console.log(this.date1);
  }

  submitForm(){
    var categories = this.blogForm.get('categories')?.value;
    var listCategory = categories.split(",");
    var list_category = [];
    for(let category of listCategory){
      if(category.trim().length === 0){
        console.log('loai')
      }else{
        list_category.push(category)
      }
    }
    var blogId = this.route.snapshot.params['id'];
    this.BlogService.deleteBlogCategory(blogId).subscribe((res: any)=>{
      console.log('ok')
    });
    
    this.ImageService.deleteBlogId(blogId).subscribe((res: any) =>{
      console.log('xoa anh')
    })

    const imgRex = /<img.*?src="(.*?)"[^>]+>/g;
    const images = [];
    let img;
    while ((img = imgRex.exec(this.ckeditorContent))) {
     	images.push(img[1]);
    }
    for(let image of images){
      let image_ = {
        imageUrl:image,
        blogId:blogId
      }
      this.ImageService.create(image_).subscribe((res: any) =>{
        console.log('da them anh')
      })
    }
    var imageName = '';
    if(this.selectedFile != null){
      imageName = this.selectedFile.name;
    }
    if(this.blogForm.get('scheduleDate')?.value == ''){
      var date = new Date().toISOString().split("T")[0];
      let blog= {
        id: blogId,
        title:this.blogForm.get('title')?.value,
        summary:this.blogForm.get('summary')?.value,
        content:this.blogForm.get('content')?.value,
        listCategory:list_category,
        views: localStorage.getItem('views'),
        status:'Đã công bố',
        createDate: localStorage.getItem('createDate'),
        updateDate: date,
        image: imageName,
        userId: localStorage.getItem('user_id')
      }
      if(blog.title == ''){
        this.message.create('error', 'Tiêu đề không được để trống');
      }else if(blog.summary == ''){
        this.message.create('error', 'Tóm tắt không được để trống');
      }else if(blog.content == ''){
        this.message.create('error', 'Nội dung không được để trống');
      }else{
        this.BlogService.update(blogId,blog).subscribe((res: any)=>{
          console.log(res);
          window.location.href = '/admin/blog/list'
        })
      }
      // console.log(blog);
      
    }else{
      var date = new Date().toISOString().split("T")[0];
      let blog= {
        id: blogId,
        title:this.blogForm.get('title')?.value,
        summary:this.blogForm.get('summary')?.value,
        content:this.blogForm.get('content')?.value,
        listCategory:list_category,
        views: localStorage.getItem('views'),
        status:'Chờ đăng bài',
        createDate: this.blogForm.get('scheduleDate')?.value.toISOString().split("T")[0],
        updateDate: date,
        image: imageName,
        userId: localStorage.getItem('user_id')
      }
      
      if(blog.title == ''){
        this.message.create('error', 'Tiêu đề không được để trống');
      }else if(blog.summary == ''){
        this.message.create('error', 'Tóm tắt không được để trống');
      }else if(blog.content == ''){
        this.message.create('error', 'Nội dung không được để trống');
      }else{
        this.BlogService.update(blogId,blog).subscribe((res: any)=>{
          console.log(res);
          window.location.href = '/admin/blog/list'
        })
      }
      // console.log(blog);
      // this.BlogService.update(blogId,blog).subscribe((res: any)=>{
      //   console.log(res);
      //   window.location.href = '/admin/blog/list'
      // })

    }  
  }

  onSelect(e: any){
    if(e.target.files){
      this.selectedFile = <File> e.target.files[0];
      this.upload();
      
    }
  }

  upload(){
    console.log(this.selectedFile.name);
    const fd = new FormData();
    fd.append('image',this.selectedFile,this.selectedFile.name);
    this.http.post(`http://localhost:8080/api/uploadimage`,fd).subscribe((res: any) =>{
      console.log(res);
    })
  }


}