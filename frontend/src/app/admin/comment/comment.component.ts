import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../services/blog.service';
import { CommentService } from 'src/app/services/comment.service';
import { CategoryService } from 'src/app/services/category.service';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzMessageService } from 'ng-zorro-antd/message';
import {  FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  constructor(
    private CommentService: CommentService,
    private BlogService:BlogService,
  ) { }

  public comments: any;
  checkList: any;
  selectedValue :string | null = null;
  statusValue:string | null = null;
  blogs: any;
  role: any;
  ngOnInit(): void {
    this.search();
    this.getAllBlog();
    this.isAdminOrAuthor();
  }

  isAdminOrAuthor(){
    this.role = localStorage.getItem('role');
  }

  search(){
    var filter = localStorage.getItem('filter')
    if(filter == 'true'){
      var blogId = localStorage.getItem('selectedValue')
      var status = localStorage.getItem('statusValue')
      console.log(blogId);
      console.log(status);
      if(blogId != ''){
        if(status != ''){
          let comment = {
            blogId : blogId,
            status : status
          }
          this.CommentService.filter(comment).subscribe((res: any) =>{
            console.log(res)
            this.comments = res;
          })
        }else{
          this.CommentService.filterByBlogId(blogId).subscribe((res: any) =>{
            this.comments = res;
          })
        }
      }else{
        if(status != ''){
          this.CommentService.filterByStatus(status).subscribe((res: any) =>{
            this.comments = res;
          })
        }else{
          this.getComment();
        }
      }
    }else{
      this.getComment();
    }
    localStorage.setItem('selectedValue', '')
    localStorage.setItem('statusValue', '')
    localStorage.setItem('filter', '')
  }

  getComment(){
    let role = localStorage.getItem('role');
    let user_id = localStorage.getItem('user_id');
    if(role == 'admin'){
      this.CommentService.getAll().subscribe((res:any) =>{
        this.comments = res;
        console.log(this.comments)
      })
    }else{
      this.CommentService.getByAuthorId(user_id).subscribe((res:any) =>{
        this.comments = res;
        console.log(this.comments)
      })
    }
    
  }

  checkbox(){
    var checklist =[]
    var checkboxAll1 = document.getElementsByTagName('input');
    var checkAllbtn = document.getElementsByClassName('checkall-btn')
    var count = 0;
    var sumCheckbox =0;
    for(var i=2; i < checkboxAll1.length; i++) {
      if(checkboxAll1[i].type == 'checkbox' ) {
        sumCheckbox++;
        if(checkboxAll1[i].checked == true){
          count++;
          checklist.push(checkboxAll1[i].value)
        }
      }
    }
    this.checkList = checklist
    console.log(this.checkList);
    if(sumCheckbox == count){
      checkboxAll1[1].checked = true;
    }else{
      checkboxAll1[1].checked = false;
    }
    if(count > 0){
      checkAllbtn[0].classList.remove('disabled')
    }else{
      checkAllbtn[0].classList.add('disabled')
    }
  }

  checkboxAll(){
    var checklist =[]
    var checkboxAll1 = document.getElementsByTagName('input')
    var count = 0
    console.log(checkboxAll1[1])

    for(var i=2; i < checkboxAll1.length; i++) {
      if(checkboxAll1[i].type == 'checkbox') {
        checkboxAll1[i].checked = checkboxAll1[1].checked;
        if(checkboxAll1[i].checked == true){
          count ++;
          checklist.push(checkboxAll1[i].value)
        }

      }
    }

    this.checkList = checklist
    console.log(this.checkList);

    var checkAllbtn = document.getElementsByClassName('checkall-btn')
    if(checkboxAll1[1].checked == true){
      checkAllbtn[0].classList.remove('disabled')
    }
    if(count == 0){
      checkAllbtn[0].classList.add('disabled')
    }
  }

  submit(){
    var select = document.getElementsByTagName('select');
    console.log(select[0].value)
    console.log(this.checkList)
    if(select[0].value == 'delete'){
      for(let a of this.checkList){
        this.deleteContent(a)
      }
    }else if(select[0].value == 'restore'){
      for(let a of this.checkList){
        this.restoreContent(a)
      }
    }else if(select[0].value == 'delete_'){
      for(let a of this.checkList){
        this.delete(a)
      }
    }else if(select[0].value == 'spam'){
      for(let a of this.checkList){
        this.addSpamSatus(a)
      }
    }else if(select[0].value == 'remove-spam'){
      for(let a of this.checkList){
        this.removeSpamSatus(a)
      }
    }
  }

  addSpamSatus(id: any){
    this.CommentService.addSpam(id).subscribe((res: any) =>{
      console.log('add thành công')
      window.location.reload()
    })
  }

  removeSpamSatus(id: any){
    this.CommentService.removeSpam(id).subscribe((res: any) =>{
      console.log('remove thành công')
      window.location.reload()
    })
  }

  deleteContent(id: any){
    this.CommentService.deleteContent(id).subscribe((res: any) =>{
      window.location.reload()
    })
  }

  restoreContent(id: any){
    this.CommentService.restoreContent(id).subscribe((res: any) =>{
      window.location.reload()
    })
  }

  delete(id: any){
    this.CommentService.delete(id).subscribe((res: any) =>{
      window.location.reload()
    })
  }

  getAllBlog(){
    let role = localStorage.getItem('role');
    let user_id = localStorage.getItem('user_id');
    if(role == 'admin'){
      this.BlogService.getAllBlog().subscribe((res: any) =>{
        this.blogs = res;
        console.log(this.blogs)
      })
    }else {
      this.BlogService.finbyAuthorId(user_id).subscribe((res: any) =>{
        this.blogs = res;
        console.log(this.blogs)
      })
    }
    
  }

  filter(){
    var a = this.selectedValue;
    localStorage.setItem('selectedValue', '')
    localStorage.setItem('statusValue', '')
    if(a != null){
      localStorage.setItem('selectedValue', a);
    }
    var b = this.statusValue
    if(b != null){
      console.log(b)
      localStorage.setItem('statusValue', b);
    }
    localStorage.setItem('filter', 'true');
    window.location.reload()
  }

  
}
