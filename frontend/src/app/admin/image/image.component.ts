import { Component, OnInit } from '@angular/core';
import { ImageService } from 'src/app/services/image.service';
import { BlogService } from 'src/app/services/blog.service';
@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {

  constructor(
    private ImageService: ImageService,
    private BlogService: BlogService
  ) { }

  public images: any;
  public blogs: any;
  selectedValue :string | null = null;

  ngOnInit(): void {
    this.search();
    this.getAllBlog();
  }

  search(){
    var filter = localStorage.getItem('filter')
    if(filter == 'true'){
      var blogId = localStorage.getItem('selectedValue')
      console.log(blogId);
      if(blogId == ''){
        this.getAllImage();
      }else{
        this.ImageService.findByBlogId(blogId).subscribe((res: any) =>{
          this.images = res;
        })
      }
    }else{
      this.getAllImage();
    }
    localStorage.setItem('selectedValue', '')
    localStorage.setItem('filter', '')
  }

  getAllBlog(){
    let role = localStorage.getItem('role');
    let user_id = localStorage.getItem('user_id');
    if(role == 'admin'){
      this.BlogService.getAllBlog().subscribe((res: any) =>{
        this.blogs = res;
        console.log(this.blogs)
      })
    }else {
      this.BlogService.finbyAuthorId(user_id).subscribe((res: any) =>{
        this.blogs = res;
        console.log(this.blogs)
      })
    }
  }

  getAllImage(){
    let role = localStorage.getItem('role');
    let user_id = localStorage.getItem('user_id');
    if(role == 'admin'){
      this.ImageService.getAllImage().subscribe((res: any)=>{
        this.images = res;
        console.log(this.images);
      })
    }else {
      this.ImageService.getByAuthorId(user_id).subscribe((res: any)=>{
        this.images = res;
        console.log(this.images);
      })
    }
    
  }

  filter(){
    var a = this.selectedValue;
    localStorage.setItem('selectedValue', '')
    if(a != null){
      localStorage.setItem('selectedValue', a);
    }
    localStorage.setItem('filter', 'true');
    window.location.reload()
  }
}
