import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  constructor(
    private UserService:UserService
  ) { }

  ngOnInit(): void {
    this.getAllUser();
  }

  checkList: any;
  users: any;

  checkbox(){
    var checklist =[]
    var checkboxAll1 = document.getElementsByTagName('input');
    var checkAllbtn = document.getElementsByClassName('checkall-btn')
    var count = 0;
    var sumCheckbox =0;
    for(var i=2; i < checkboxAll1.length; i++) {
      if(checkboxAll1[i].type == 'checkbox' ) {
        sumCheckbox++;
        if(checkboxAll1[i].checked == true){
          count++;
          checklist.push(checkboxAll1[i].value)
        }
      }
    }
    this.checkList = checklist
    console.log(this.checkList);
    if(sumCheckbox == count){
      checkboxAll1[1].checked = true;
    }else{
      checkboxAll1[1].checked = false;
    }
    if(count > 0){
      checkAllbtn[0].classList.remove('disabled')
    }else{
      checkAllbtn[0].classList.add('disabled')
    }
  }

  checkboxAll(){
    var checklist =[]
    var checkboxAll1 = document.getElementsByTagName('input')
    var count = 0
    console.log(checkboxAll1[1])

    for(var i=2; i < checkboxAll1.length; i++) {
      if(checkboxAll1[i].type == 'checkbox') {
        checkboxAll1[i].checked = checkboxAll1[1].checked;
        if(checkboxAll1[i].checked == true){
          count ++;
          checklist.push(checkboxAll1[i].value)
        }

      }
    }

    this.checkList = checklist
    console.log(this.checkList);

    var checkAllbtn = document.getElementsByClassName('checkall-btn')
    if(checkboxAll1[1].checked == true){
      checkAllbtn[0].classList.remove('disabled')
    }
    if(count == 0){
      checkAllbtn[0].classList.add('disabled')
    }
  }

  submit(){
    var select = document.getElementsByTagName('select');
    console.log(select[0].value)
    console.log(this.checkList)
    if(select[0].value == 'delete'){
      for(let a of this.checkList){
        this.delete(a);
      }
      window.location.reload()
    }else if(select[0].value == 'author'){
      for(let a of this.checkList){
        this.setroleAuthor(a)
      }
      window.location.reload()
    }else if(select[0].value == 'viewer'){
      for(let a of this.checkList){
        this.removeroleAuthor(a)
      }
      window.location.reload()
    }
  }

  getAllUser(){
    this.UserService.getAllUser().subscribe((res: any) =>{
      console.log(res);
      this.users = res;
    })
  }

  delete(id: any){
    this.UserService.delete(id).subscribe((res: any) =>{
      console.log(res);
    })
    
  }

  setroleAuthor(id: any){
    this.UserService.setroleAuthor(id).subscribe((res: any) =>{
      console.log(res);
      
    })
    
  }

  removeroleAuthor(id: any){
    this.UserService.removeroleAuthor(id).subscribe((res: any) =>{
      console.log(res);
      
    })
    
  }

}
