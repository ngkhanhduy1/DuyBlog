import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { SocialService } from 'src/app/services/social-auth.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { SocialAuthService } from "@abacritt/angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "@abacritt/angularx-social-login";
import { data } from 'jquery';
import { ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  auth2: any;
  @ViewChild('loginRef', { static: true }) loginElement!: ElementRef;

  constructor(
    private AuthService:AuthService,
    private message: NzMessageService,
    private http: HttpClient,
    private socialService: SocialAuthService,
    private SocialAuth: SocialService
    ) { }

  ngOnInit(): void {
    this.googleAuthSDK();
  }

  isAdmin = false

  loginForm = new UntypedFormGroup({
    username: new UntypedFormControl('',),
    password: new UntypedFormControl('',)
  });

  login(){
    let login = {
      "username" : this.loginForm.value.username,
      "password" : this.loginForm.value.password,
    }
    console.log(login)
    this.AuthService.login(login).subscribe({
      next: (res:any) => {
        localStorage.setItem('token',res.id_token);
        this.getRole(res.id_token);
      },
      error: () => (
        this.message.create('error', `Tài khoản hoặc mật khẩu không chính xác `)
      ),
    });
  }

  getRole(res : any){
    var header = {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${res}`)
    }
    this.http.get(`http://localhost:8080/api/authenticate`,header).subscribe((res)=>{
      console.log(res.valueOf());
      var a = res.valueOf().toString();
      console.log(a.split(",").length)
      if(a.split(",").length == 3){
        localStorage.setItem('role','admin')
        window.location.href = '/admin/analytic'
      }else if(a.split(",").length == 2){
        localStorage.setItem('role','author')
        window.location.href = '/admin/blog/list'
        console.log('author')
      }else {
        localStorage.setItem('role','user')
        window.location.href = '/home'
      }
    });
  }

  signInWithFB(): void {
    this.socialService.signIn(FacebookLoginProvider.PROVIDER_ID).then(
      data => {
        localStorage.setItem('photoUrl',data.photoUrl)
        console.log(data)
        var token = {
          value : data.authToken
        }
        this.SocialAuth.facebook(token).subscribe((res: any) =>{
          localStorage.setItem('token',res.id_token);
          this.getRole(res.id_token);
        })
      }
    );
  }

  callLogin() {

    this.auth2.attachClickHandler(this.loginElement.nativeElement, {},
      (googleAuthUser: any) => {

        //Print profile details in the console logs

        let profile = googleAuthUser.getBasicProfile();
        console.log(googleAuthUser);
        console.log('Token || ' + googleAuthUser.getAuthResponse().id_token);
        var token = {
          value : googleAuthUser.getAuthResponse().id_token
        }
        this.SocialAuth.google(token).subscribe((res: any) =>{
          localStorage.setItem('token',res.id_token);
          this.getRole(res.id_token);
        })

      }, (error: any) => {
        alert(JSON.stringify(error, undefined, 2));
      });

  }

  googleAuthSDK() {

    (<any>window)['googleSDKLoaded'] = () => {
      (<any>window)['gapi'].load('auth2', () => {
        this.auth2 = (<any>window)['gapi'].auth2.init({
          client_id: '53498111056-j16luq7ujs7hecj7c63324iuhf2uqoc0.apps.googleusercontent.com',
          plugin_name:'login',
          cookiepolicy: 'https://localhost:4200/',
          scope: ''
        });
        this.callLogin();
      });
    }

    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) { return; }
      js = d.createElement('script');
      js.id = id;
      js.src = "https://apis.google.com/js/platform.js?onload=googleSDKLoaded";
      fjs?.parentNode?.insertBefore(js, fjs);
    }(document, 'script', 'google-jssdk'));
  }

}
