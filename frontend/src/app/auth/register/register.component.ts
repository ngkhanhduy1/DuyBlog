import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { AccountService } from '../../services/account.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-login',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(
    private AccountService:AccountService,
    private message: NzMessageService
    ) { }
  
  public isRegisterSuccess: any
  
  ngOnInit(): void {
    this.isRegisterSuccess = localStorage.getItem('isRegisterSuccess')
    if(this.isRegisterSuccess == 'true'){
      this.message.create('success',`Đăng kí thành công`)
      localStorage.setItem('isRegisterSuccess', "false")
    }
  }

  registerForm = new UntypedFormGroup({
    login: new UntypedFormControl('',),
    password: new UntypedFormControl('',),
    firstName: new UntypedFormControl('',),
    lastName: new UntypedFormControl('',),
    email: new UntypedFormControl('',)
  });

  register(){
    console.log(this.registerForm.value);
    if(this.registerForm.value.firstname == ''){
      this.message.create('error', 'Họ không được bỏ trống')
    }else if(this.registerForm.value.lastname == ''){
      this.message.create('error', 'Tên không được bỏ trống')
    }else{
      this.AccountService.register(this.registerForm.value).subscribe((res: any) =>{
        console.log(res)
        if(res == 'Đăng kí thành công'){
          location.href ='/login'
        } else{
          this.message.create('error', res)
        }
      });
    }
    
    
  }

}
