import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router'
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private Router:Router,
    private AuthService:AuthService
  ){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      this.AuthService.getRole().subscribe({
        next: (res: any) => {
          let isAdmin = false;
          for(let role of res) {
            if(role == 'ROLE_ADMIN' || role == 'ROLE_AUTHOR'){
              isAdmin = true
            }
          }
          if(isAdmin == false){
            location.href ="/login"
          }
        },
        error: ()=> {
          location.href ="/login"
        }
      })
    return true;
  }
}
