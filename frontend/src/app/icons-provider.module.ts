import { NgModule } from '@angular/core';
import { NZ_ICONS, NzIconModule } from 'ng-zorro-antd/icon';





@NgModule({
  imports: [NzIconModule],
  exports: [NzIconModule],
  providers: [
    
  ]
})
export class IconsProviderModule {
}
