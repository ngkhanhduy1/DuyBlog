import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export const DATE_FORMAT = 'YYYY-MM-DD';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  protected resourceUrl = environment.GATEWAY_URL + 'api';

  constructor(
    private http: HttpClient
  ) { }

  private token = localStorage.getItem('token')
  private header = {
    headers: new HttpHeaders()
      .set('Authorization', `Bearer ${this.token}`)
  }

  register(account: any): Observable<any> {
    return this.http.post(`${this.resourceUrl}/register`, account, {responseType:'text'})
  }

  public getAllAccount() {
    return this.http.get(`${this.resourceUrl}/admin/users`, this.header);
  }

  delete(login: any){
    return this.http.delete<any>(`${this.resourceUrl}/admin/users/${login}`, this.header)
  }

}
