import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';

const resourceUrl = `${environment.GATEWAY_URL}`;

@Injectable({
  providedIn: 'root'
})
export class AuthHttpService {

  constructor(
    private http: HttpClient
  ) { }
  

  private token = localStorage.getItem('token')
  private header = {
    headers: new HttpHeaders()
      .set('Authorization', `Bearer ${this.token}`)
  }

  login(login: any): Observable<any> {
    return this.http.post<any>(`${resourceUrl}api/authenticate`, login);
  }

  getUserByToken(): Observable<any> {
    console.log(this.token)
    return this.http.get(`${resourceUrl}api/account`,this.header);
  }

  getRole(): Observable<any> {
    console.log(this.header);
    return this.http.get(`${resourceUrl}api/authenticate`,this.header);
  }
}
