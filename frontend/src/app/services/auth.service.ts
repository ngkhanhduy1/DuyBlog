import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { AuthHttpService } from "./auth-http.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private authHttpService: AuthHttpService,
    private router: Router
  ) {
    this.isLoadingSubject = new BehaviorSubject<boolean>(false);
    this.isLoading$ = this.isLoadingSubject.asObservable();

  }


  isLoadingSubject!: BehaviorSubject<boolean>;
  isLoading$: Observable<boolean>;
  currentUserSubject!: BehaviorSubject<any>;


  login(login: any): Observable<any> {
    return this.authHttpService.login(login)
  }

  getUserByToken(): Observable<any> {
    return this.authHttpService.getUserByToken();
  }

  logout() {
    localStorage.removeItem('token');
    window.location.reload();
  }

  getRole(): Observable<any> {
    var token = localStorage.getItem('token')
    console.log(token)
    return this.authHttpService.getRole();
  }
}
