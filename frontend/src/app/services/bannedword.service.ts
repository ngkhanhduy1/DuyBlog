import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class BannedwordService {

  protected resourceUrl = environment.GATEWAY_URL + 'api/bannedwords';
  constructor(
    private http: HttpClient
  ) { }

  private token = localStorage.getItem('token')
  private header = {
    headers: new HttpHeaders()
      .set('Authorization', `Bearer ${this.token}`)
  }

  public getAll() {
    return this.http.get(this.resourceUrl, this.header);
  }

  create(word: any): Observable<any> {
    return this.http.post<any>(this.resourceUrl, word, this.header)
  }

  delete(id: any){
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, this.header)
  }
}
