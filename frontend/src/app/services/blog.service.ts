import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export const DATE_FORMAT = 'YYYY-MM-DD';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  protected resourceUrl = environment.GATEWAY_URL + 'api/blogs';
  protected blogCategoryUrl = environment.GATEWAY_URL + 'api/blogcategories';

  constructor(
    private http: HttpClient
  ) { }

  private token = localStorage.getItem('token')
  private header = {
    headers: new HttpHeaders()
      .set('Authorization', `Bearer ${this.token}`)
  }

  public getAllBlog() {
    return this.http.get(this.resourceUrl, this.header);
  }

  public getRandomBlog() {
    return this.http.get(`${this.resourceUrl}/random`, this.header);
  }

  public getPublicBlog() {
    return this.http.get(`${this.resourceUrl}/getpublicblog`, this.header);
  }


  delete(id: any){
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, this.header)
  }

  findById(id: any){
    return this.http.get(`${this.resourceUrl}/${id}`, this.header);
  }

  create(blog: any): Observable<any> {
    return this.http.post<any>(this.resourceUrl, blog, this.header)
  }

  update(id: any,blog: any):Observable<any>{
    return this.http.put<any>(`${this.resourceUrl}/${id}`,blog, this.header);
  }

  top5orderByView(){
    return this.http.get(`${this.resourceUrl}/order`, this.header)
  }

  count(){
    return this.http.get(`${this.resourceUrl}/count`, this.header);
  }

  sumView(){
    return this.http.get(`${this.resourceUrl}/sumview`, this.header);
  }

  search(form: any): Observable<any> {
    return this.http.post<any>(`${this.resourceUrl}/search`, form, this.header)
  }

  addCategory(form: any): Observable<any> {
    return this.http.post<any>(`${this.resourceUrl}/addcategory`, form, this.header)
  }

  deleteBlogCategory(id: any){
    return this.http.delete<any>(`${this.blogCategoryUrl}/deletebyblogid/${id}`, this.header)
  }

  viewup(id: any){
    return this.http.put<any>(`${this.resourceUrl}/viewup/${id}`, this.header)
  }

  finbyAuthorId(id: any){
    return this.http.get(`${this.resourceUrl}/findbyauthorid/${id}`, this.header);
  }

  setPrivate(id: any){
    return this.http.put(`${this.resourceUrl}/setprivate/${id}`, this.header);
  }

  setPublic(id: any){
    return this.http.put(`${this.resourceUrl}/setpublic/${id}`, this.header);
  }

  suggestBlogByAuthor(id: any){
    return this.http.get(`${this.resourceUrl}/suggestbyAuthor/${id}`, this.header);
  }

  findByCategoryId(id: any){
    return this.http.get(`${this.resourceUrl}/findbycategoryid/${id}`, this.header);
  }
}
