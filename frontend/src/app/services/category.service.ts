import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export const DATE_FORMAT = 'YYYY-MM-DD';


@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  protected resourceUrl = environment.GATEWAY_URL + 'api/categories';

  constructor(
    private http: HttpClient
  ) { }

  private token = localStorage.getItem('token')
  private header = {
    headers: new HttpHeaders()
      .set('Authorization', `Bearer ${this.token}`)
  }
  

  public getAllCategoryName() {
    return this.http.get(`${this.resourceUrl}/name`, this.header);
  }

  public getAllCategory() {
    return this.http.get(this.resourceUrl, this.header);
  }

  delete(id: any){
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, this.header)
  }

  findById(id: any){
    return this.http.get(`${this.resourceUrl}/${id}`, this.header);
  }

  create(category: any): Observable<any> {
    return this.http.post<any>(this.resourceUrl, category, this.header)
  }

  update(id: any,category: any):Observable<any>{
    return this.http.put<any>(`${this.resourceUrl}/${id}`,category, this.header);
  }
}
