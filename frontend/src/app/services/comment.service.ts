import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export const DATE_FORMAT = 'YYYY-MM-DD';


@Injectable({
  providedIn: 'root'
})
export class CommentService {

  protected resourceUrl = environment.GATEWAY_URL + 'api/comments';

  constructor(
    private http: HttpClient
  ) { }

  private token = localStorage.getItem('token')
  private header = {
    headers: new HttpHeaders()
      .set('Authorization', `Bearer ${this.token}`)
  }
  

  
  findByBlogId(id: any){
    return this.http.get(`${this.resourceUrl}/findbyblogid/${id}`, this.header);
  }

  create(comment: any,id: any): Observable<any> {
    return this.http.post(`${this.resourceUrl}/${id}`, comment, {responseType: 'text'} )
  }

  count(){
    return this.http.get(`${this.resourceUrl}/count`, this.header);
  }

  getAll(){
    return this.http.get(this.resourceUrl, this.header);
  }

  addSpam(id :any){
    return this.http.put(`${this.resourceUrl}/addspam/${id}`, this.header )
  }

  removeSpam(id :any){
    return this.http.put(`${this.resourceUrl}/removespam/${id}`, this.header )
  }

  deleteContent(id :any){
    return this.http.put(`${this.resourceUrl}/deletecontent/${id}`, this.header )
  }

  restoreContent(id :any){
    return this.http.put(`${this.resourceUrl}/restorecontent/${id}`, this.header )
  }

  delete(id: any){
    return this.http.delete(`${this.resourceUrl}/${id}`, {responseType:'text'} )
  }

  filterByBlogId(id: any){
    return this.http.get(`${this.resourceUrl}/filterbyblogid/${id}`, this.header);
  }

  filterByStatus(status: any){
    return this.http.get(`${this.resourceUrl}/filterbystatus/${status}`, this.header);
  }

  filter(comment: any){
    return this.http.post(`${this.resourceUrl}/filter`, comment);
  }

  getByAuthorId(id: any){
    return this.http.get(`${this.resourceUrl}/getbyauthorid/${id}`, this.header);
  }
}
