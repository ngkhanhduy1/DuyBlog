import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  protected resourceUrl = environment.GATEWAY_URL + 'api/images';

  constructor(
    private http: HttpClient
  ) { }

  private token = localStorage.getItem('token')
  private header = {
    headers: new HttpHeaders()
      .set('Authorization', `Bearer ${this.token}`)
  }

  deleteBlogId(id: any){
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, this.header)
  }

  create(image: any): Observable<any> {
    return this.http.post<any>(this.resourceUrl, image, this.header)
  }

  getAllImage() {
    return this.http.get(this.resourceUrl, this.header);
  }

  findByBlogId(id: any){
    return this.http.get(`${this.resourceUrl}/findbyblogid/${id}`,this.header)
  }

  getByAuthorId(id: any){
    return this.http.get(`${this.resourceUrl}/getbyauthorid/${id}`, this.header);
  }

}
