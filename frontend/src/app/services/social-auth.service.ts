import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const header = {headers: new HttpHeaders({'Content-Type' : 'application/json'})};

@Injectable({
  providedIn: 'root'
})
export class SocialService {

  protected resourceUrl = environment.GATEWAY_URL + 'api/';

  constructor(private httpClient: HttpClient) { }


  public facebook(token: any): Observable<any> {
    return this.httpClient.post<any>(this.resourceUrl + 'facebook', token, header);
  }

  public google(token: any): Observable<any> {
    return this.httpClient.post<any>(this.resourceUrl + 'google', token, header);
  }

}