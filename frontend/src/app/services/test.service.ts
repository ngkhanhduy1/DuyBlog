import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export const DATE_FORMAT = 'YYYY-MM-DD';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  protected resourceUrl = environment.GATEWAY_URL + 'api/test';

  constructor(
    private http: HttpClient
  ) { }

  private token = localStorage.getItem('token')
  private header = {
    headers: new HttpHeaders()
      .set('Authorization', `Bearer ${this.token}`)
  }

  public getAll(id: any) {
    return this.http.get(`${this.resourceUrl}/${id}`, this.header);
  }

  create(word: any,id: any): Observable<any> {
    return this.http.post<any>(`${this.resourceUrl}/${id}`, word, this.header)
  }
}
