import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  protected resourceUrl = environment.GATEWAY_URL + 'api/admin/users';

  constructor(
    private http: HttpClient
  ) { }

  private token = localStorage.getItem('token')
  private header = {
    headers: new HttpHeaders()
      .set('Authorization', `Bearer ${this.token}`)
  }

  public getAllUser() {
    return this.http.get(this.resourceUrl, this.header);
  }

  public delete(id:any){
    return this.http.delete(`${this.resourceUrl}/${id}`, this.header);
  }

  setroleAuthor(id: any){
    return this.http.put(`${this.resourceUrl}/setrole/${id}`, this.header);
  }

  removeroleAuthor(id: any){
    return this.http.put(`${this.resourceUrl}/removerole/${id}`, this.header);
  }

  public getAllAuthor() {
    return this.http.get(`${this.resourceUrl}/authors`, this.header);
  }


}
