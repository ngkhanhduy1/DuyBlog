import { Component, OnInit, ViewEncapsulation  } from '@angular/core';
import { BlogService } from '../../services/blog.service';
import { CommentService } from 'src/app/services/comment.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DetailComponent implements OnInit {

  constructor(
    private AuthService: AuthService,
    private CommentService: CommentService,
    private BlogService:BlogService,
    private route: ActivatedRoute,
    private message: NzMessageService,
  ) { }
  
  public date = new Date().toISOString().split("T")[0];
  public id: any;
  public authorId: any;
  public currentUserId: any;
  public blog: any;
  public content: any;
  public randomBlogs: any;
  public comments: any;
  public suggestBlog1: any;
  public suggestBlog2: any;
  isAuthenticated = false;
  inputValue: string | null = null;
  url: string | null = null;
  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.url = location.href;
    console.log(this.url);
    this.viewUp(this.id);
    this.suggestByAuthor(this.id);
    this.suggestByView()
    this.getCurrentUserId();
    this.findById(this.id);
    this.getRandomBlog();
    this.getCommentByBlogId(this.id);

  }

  getCurrentUserId(){
    this.AuthService.getUserByToken().subscribe((res: any) =>{
      if(res){
        this.isAuthenticated = true,
        this.currentUserId = res
      }
    })
  }

  getRandomBlog(){
    this.BlogService.getRandomBlog().subscribe((res: any) =>{
      this.randomBlogs = res;
    })
  }

  getCommentByBlogId(id: any){
    this.CommentService.findByBlogId(id).subscribe((res: any) =>{
      this.comments = res;
      console.log(this.comments)
    })
  }

  findById(id: any){
    this.BlogService.findById(id).subscribe((res:any) =>{
      this.blog = res;
      this.content = res.content;
      this.authorId = res.user.id;
      localStorage.setItem('authorId',res.user.id);
      console.log(this.blog); 
    })
  }

  onEnter(){
    if(this.inputValue?.trim.length === 0){
      var date = new Date().toISOString().split("T")[0];
      let blog = {
        userId: this.currentUserId.id,
        blogId: this.id,
        status: 'public',
        contentBackup:this.inputValue.trim(),
        content: this.inputValue.trim(),
        createDate: date
      }
      let authorId = localStorage.getItem('authorId');
      this.CommentService.create(blog,authorId).subscribe((res: any) =>{
        if(res == 'thêm thành công'){
          window.location.reload()
        }else{
          this.message.create('error',res)
        }
      })
    }
  }

  viewUp(id: any){
    this.BlogService.viewup(id).subscribe((res: any) =>{
      console.log('da tang view')
    })
  }

  suggestByAuthor(id: any){
    this.BlogService.suggestBlogByAuthor(id).subscribe((res: any) =>{
      this.suggestBlog1 = res;
      console.log(this.suggestBlog1)
    })
  }

  suggestByView(){
    this.BlogService.top5orderByView().subscribe((res: any) =>{
      this.suggestBlog2 = res;
    })
  }

  categoryClick(name: any){
    localStorage.setItem('searchBy','categoryName');
    localStorage.setItem('categoryName',name);
    location.href='/search'
    console.log(name)
  }

}
