import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../services/blog.service';
import { UserService } from 'src/app/services/user.service';
import { CategoryService} from '../../services/category.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private BlogService:BlogService,
    private UserService:UserService,
    private CategoryService: CategoryService
  ) { }
  
  public blogs: any;
  public newestBlog: any;
  public authors: any;
  public categories: any;
  public suggestBlog: any;
  inputValue: string | null = null;
  ngOnInit(): void {
    this.getAllBlog();
    this.getAllAuthor();
    this.getAllCategory();
    this.suggestByView();
  }

  getAllBlog(){
    this.BlogService.getPublicBlog().subscribe((res: any)=>{
      this.blogs = res;
      this.newestBlog = this.blogs[this.blogs.length -1]
      console.log(this.blogs)
    })
  }
  suggestByView(){
    this.BlogService.top5orderByView().subscribe((res: any) =>{
      this.suggestBlog = res;
      console.log(res)
    })
  }

  onEnter(){
    localStorage.setItem('searchBy','keyword')
    if(this.inputValue != null){
      localStorage.setItem('keyword',this.inputValue)
    }
    window.location.href = '/search';
  }

  getAllAuthor(){
    this.UserService.getAllAuthor().subscribe((res: any)=>{
      this.authors = res;
      console.log(res)
    })
  }

  authorClick(id: any, firstName: any, lastName: any){
    localStorage.setItem('searchBy','author');
    localStorage.setItem('authorId',id);
    localStorage.setItem('authorName',firstName +' '+ lastName);
    location.href='/search'
    console.log(id)
  }

  getAllCategory(){
    this.CategoryService.getAllCategory().subscribe((res: any)=>{
      this.categories = res;
      console.log(res);
    })
  }

  categoryClick(id: any, categoryName: any){
    localStorage.setItem('searchBy','category');
    localStorage.setItem('categoryName', categoryName)
    localStorage.setItem('categoryId',id);
    location.href='/search'
    console.log(id)
  }

}
