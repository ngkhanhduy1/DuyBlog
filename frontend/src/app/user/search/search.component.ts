import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../services/blog.service';
import { UserService } from 'src/app/services/user.service';
import { CategoryService} from '../../services/category.service'

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  constructor(
    private BlogService:BlogService,
    private UserService:UserService,
    private CategoryService: CategoryService
  ) { }

  public blogs: any;
  public authors: any;
  public categories: any;
  public text: any;
  inputValue: string | null = null;

  ngOnInit(): void {
    this.search();
    this.getAllAuthor();
    this.getAllCategory();
  }

  search(){
    var searchBy = localStorage.getItem('searchBy')
    if(searchBy == 'keyword'){
      this.text = 'Kết Quả tìm kiếm theo từ khóa: ';
      var keyword = localStorage.getItem('keyword');
      this.text = this.text + keyword;
      console.log(keyword);
      let form = {
        keyword:keyword,
        categories:[]
      }
      this.BlogService.search(form).subscribe((res: any)=>{
        console.log(res);
        this.blogs = res;
      })
    }else if(searchBy == 'author'){
      this.text = 'Kết Quả tìm kiếm theo tác giả: ';
      var authorId = localStorage.getItem('authorId')
      var authorName = localStorage.getItem('authorName')
      this.text = this.text+authorName
      this.BlogService.finbyAuthorId(authorId).subscribe((res: any)=>{
        console.log(res);
        this.blogs = res;
      })
    }else if(searchBy == 'category'){
      this.text = 'Kết Quả tìm kiếm theo nhãn: ';
      var categoryId = localStorage.getItem('categoryId')
      var categoryName = localStorage.getItem('categoryName')
      this.text = this.text + categoryName
      this.BlogService.findByCategoryId(categoryId).subscribe((res: any)=>{
        console.log(res);
        this.blogs = res;
      })
    }else if(searchBy == 'categoryName'){
      this.text = 'Kết Quả tìm kiếm theo nhãn: ';
      var categoryName = localStorage.getItem('categoryName')
      this.text = this.text + categoryName;
      var part = categoryName?.split(",");
      let form = {
        categories:part
      }
      this.BlogService.search(form).subscribe((res: any)=>{
        console.log(res);
        this.blogs = res;
      })
    }else if(searchBy == 'view'){
      this.text = 'Bài viết nhiều lượt xem:';
      this.BlogService.top5orderByView().subscribe((res: any)=>{
        console.log(res);
        this.blogs = res;
      })
    }
  }

  onEnter(){
    if(this.inputValue != null){
      localStorage.setItem('keyword',this.inputValue)
    }
    window.location.href = '/search';
  }

  getAllAuthor(){
    this.UserService.getAllAuthor().subscribe((res: any)=>{
      this.authors = res;
      console.log(res)
    })
  }

  authorClick(id: any,firstName: any, lastName: any){
    localStorage.setItem('searchBy','author');
    localStorage.setItem('authorName',firstName +' '+ lastName);
    localStorage.setItem('authorId',id);
    location.href='/search'
    console.log(id)
  }

  getAllCategory(){
    this.CategoryService.getAllCategory().subscribe((res: any)=>{
      this.categories = res;
      console.log(res);
    })
  }

  categoryClick(id: any, categoryName: any){
    localStorage.setItem('searchBy','category');
    localStorage.setItem('categoryName', categoryName)
    localStorage.setItem('categoryId',id);
    location.href='/search'
    console.log(id)
  }

}
