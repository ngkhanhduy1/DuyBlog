import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { SocialAuthService } from "@abacritt/angularx-social-login";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  constructor(
    private AuthService: AuthService,
    private SocialAuthService: SocialAuthService
  ) { }
  
  isAuthenticated = false;
  public currentAccount: any;
  public photoUrl: any;

  ngOnInit(): void {
    this.getUserByToken()
    this.photoUrl = localStorage.getItem('photoUrl');
  }

  getUserByToken(){
    this.AuthService.getUserByToken().subscribe((res: any)=>{
      if(res){
        this.isAuthenticated = true,
        this.currentAccount = res,
        localStorage.setItem('user_id',res.id)
      }
    })
  }

  logout(){
    this.SocialAuthService.signOut(true);
    this.AuthService.logout()
  }

  onClick(){
    localStorage.setItem('searchBy','view');
    location.href='/search'
    
  }

}
