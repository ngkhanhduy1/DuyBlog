import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import { HomeComponent } from './home/home.component';
import { DetailComponent } from './detail/detail.component';
import { UserComponent } from './user.component';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { SearchComponent } from './search/search.component';

const pagesRoutes: Routes = [
  {path: '', component: UserComponent,children:[
    {
      path:'home',
      component: HomeComponent
    },
    {
      path:'detail/:id',
      component:DetailComponent
    },
    {
      path:'blog/:id',
      component:DetailComponent
    },
    {
      path:'search',
      component:SearchComponent
    }
  ]}
]

@NgModule({
  declarations: [
    HomeComponent,
    DetailComponent,
    SearchComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NzMessageModule,
    RouterModule.forChild(pagesRoutes),
    NgxPaginationModule,
  ]
})
export class UserModule { }
